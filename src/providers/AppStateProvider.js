
import React, { useContext, useState, useEffect } from 'react';
import { VideoPriorityBasedPolicy } from 'amazon-chime-sdk-js';
import { Layout, VideoFiltersCpuUtilization } from '../types';
import { useLogger } from 'amazon-chime-sdk-component-library-react';
import { useLocation } from 'react-router-dom';

const AppStateContext = React.createContext(null);

export function useAppState() {
  const state = useContext(AppStateContext);
  if (!state) {
    throw new Error('useAppState must be used within AppStateProvider');
  }

  return state;
}


// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function AppStateProvider({ children }) {
  const logger = useLogger();
  const location = useLocation();
  const query = new URLSearchParams(location.search);

  const [meetingId, setMeetingId] = useState(query.get('meetingId') || '');
  const [region, setRegion] = useState('us-west-2');
  const [joinInfo, setJoinInfo] = useState(undefined);
  // const [layout, setLayout] = useState(Layout.Featured);
  const [localUserName, setLocalUserName] = useState('');
  const [isWebAudioEnabled, setIsWebAudioEnabled] = useState(true);
  const [priorityBasedPolicy, setPriorityBasedPolicy] = useState(undefined);
  const [enableSimulcast, setEnableSimulcast] = useState(false);
  const [keepLastFrameWhenPaused, setKeepLastFrameWhenPaused] = useState(false);
  const [isEchoReductionEnabled, setIsEchoReductionEnabled] = useState(true);
  // const [capturePipeline, setMediaCapturePipeline] = useState({});
  const [theme, setTheme] = useState(() => {
    const storedTheme = localStorage.getItem('theme');
    return storedTheme || 'light';
  });
  const [videoTransformCpuUtilization, setCpuPercentage] = useState(VideoFiltersCpuUtilization.CPU40Percent);
  const [imageBlob, setImageBlob] = useState(undefined);
  const [skipDeviceSelection, setSkipDeviceSelection] = useState(false);
  const [localAttendee, setLocalAttendee] = useState("");
  const [cameraEnable, setCameraEnable] = useState(true);
  const [micMute, setMicMute] = useState(true);
  const [muteSpeaker, setMuteSpeaker] = useState(true);
  // const [transcriptionTotal, setTranscriptionTotal] = useState([]);
  // // const [startRecord, setStartRecord] = useState(false);
  // const [startTranscriptFun, setStartTranscriptFun] = useState(false);
  // const [showRoster, setShowRoster] = useState(false);
  // const [showChat, setShowChat] = useState(false);

  useEffect(() => {
    /* Load a canvas that will be used as the replacement image for Background Replacement */
    async function loadImage() {
      const canvas = document.createElement('canvas');
      canvas.width = 500;
      canvas.height = 500;
      const ctx = canvas.getContext('2d');
      if (ctx !== null) {
        const grd = ctx.createLinearGradient(0, 0, 250, 0);
        grd.addColorStop(0, '#000428');
        grd.addColorStop(1, '#004e92');
        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, 500, 500);
        canvas.toBlob(function (blob) {
          if (blob !== null) {
            console.log('loaded canvas', canvas, blob);
            setImageBlob(blob);
          }
        });
      }
    }
    loadImage();
  }, []);

  const toggleTheme = () => {
    if (theme === 'light') {
      setTheme('dark');
    } else {
      setTheme('light');
    }
  };

  const toggleMeetingJoinDeviceSelection = () => {
    setSkipDeviceSelection((current) => !current);
  };

  const toggleWebAudio = () => {
    setIsWebAudioEnabled((current) => !current);
  };

  const toggleSimulcast = () => {
    setEnableSimulcast((current) => !current);
  };

  const togglePriorityBasedPolicy = () => {
    if (priorityBasedPolicy) {
      setPriorityBasedPolicy(undefined);
    } else {
      setPriorityBasedPolicy(new VideoPriorityBasedPolicy(logger));
    }
  };

  const toggleKeepLastFrameWhenPaused = () => {
    setKeepLastFrameWhenPaused((current) => !current);
  };

  const setCpuUtilization = (filterValue) => {
    setCpuPercentage(filterValue);
  };

  const setBlob = (imageBlob) => {
    setImageBlob(imageBlob);
  };

  const toggleEchoReduction = () => {
    setIsEchoReductionEnabled((current) => !current);
  };

  const providerValue = {
    meetingId,
    localUserName,
    theme,
    isWebAudioEnabled,
    videoTransformCpuUtilization,
    imageBlob,
    isEchoReductionEnabled,
    region,
    // layout,
    joinInfo,
    enableSimulcast,
    priorityBasedPolicy,
    keepLastFrameWhenPaused,
    toggleTheme,
    toggleWebAudio,
    togglePriorityBasedPolicy,
    toggleKeepLastFrameWhenPaused,
    toggleSimulcast,
    setCpuUtilization,
    toggleEchoReduction,
    // setLayout,
    setJoinInfo,
    setMeetingId,
    setLocalUserName,
    setRegion,
    setBlob,
    skipDeviceSelection,
    toggleMeetingJoinDeviceSelection,

    setLocalAttendee,
    localAttendee,
    cameraEnable, setCameraEnable, micMute, setMicMute, muteSpeaker, setMuteSpeaker,
    // transcriptionTotal, setTranscriptionTotal,
    // startRecord, setStartRecord,
    // startTranscriptFun, setStartTranscriptFun,
    // showRoster, setShowRoster, showChat, setShowChat
  };

  return <AppStateContext.Provider value={providerValue}>{children}</AppStateContext.Provider>;
}
