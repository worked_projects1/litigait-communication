
export var DataMessagesActionType;

(function (DataMessagesActionType) {
  DataMessagesActionType[DataMessagesActionType["ADD"] = 0] = "ADD";
})(DataMessagesActionType || (DataMessagesActionType = {}));

export const initialState = {
  messages: [],
};

export function reducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DataMessagesActionType.ADD:
      return { messages: [...state.messages, payload] };
    default:
      throw new Error('Incorrect action in DataMessagesProvider reducer');
  }
}
