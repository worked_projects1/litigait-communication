
import React, { useState, ReactNode } from 'react';

const context = React.createContext({
  errorMessage: '',
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  updateErrorMessage: (_errorMessage) => { },
});

export function getErrorContext() {
  return context;
}

export default function ErrorProvider({ children }) {
  const [errorMessage, setErrorMessage] = useState('');
  const ErrorContext = getErrorContext();

  const updateErrorMessage = (message) => {
    setErrorMessage(message);
  };

  const providerValue = {
    errorMessage,
    updateErrorMessage,
  };
  return (
    <ErrorContext.Provider value={providerValue}>
      {children}
    </ErrorContext.Provider>
  );
}
