
import React, { createContext, useContext, useEffect, useReducer, useState } from 'react';
import {
  useActiveSpeakersState,
  useAudioVideo,
  useContentShareState,
  useLocalVideo,
  useMeetingManager,
  useRosterState,
} from 'amazon-chime-sdk-component-library-react';
import { Layout } from '../../types';
import { useAppState } from '../AppStateProvider';
import {
  Controls,
  initialState,
  reducer,
  State,
  VideoTileGridAction,
} from './state';
import { DefaultModality } from 'amazon-chime-sdk-js';

const VideoTileGridStateContext = createContext(undefined);
const VideoTileGridControlContext = createContext(undefined);

const VideoTileGridProvider = ({ children }) => {
  const { priorityBasedPolicy, joinInfo } = useAppState();
  const meetingManager = useMeetingManager();
  const audioVideo = useAudioVideo();
  // const activeSpeakers = useActiveSpeakersState();
  const { roster } = useRosterState();
  // const { layout, setLayout } = useAppState();
  const { isVideoEnabled } = useLocalVideo();
  const { isLocalUserSharing, sharingAttendeeId } = useContentShareState();
  const [state, dispatch] = useReducer(reducer, initialState);
  const [layout, setLayout] = useState(Layout.Featured);

  useEffect(() => {
    dispatch({
      type: VideoTileGridAction.UpdateAttendeeStates,
      payload: { roster },
    });
  }, [roster]);

  // useEffect(() => {
  //   dispatch({
  //     type: VideoTileGridAction.UpdateActiveSpeakers,
  //     payload: {
  //       activeSpeakers,
  //     },
  //   });
  // }, [activeSpeakers]);

  useEffect(() => {
    if (!audioVideo) {
      return;
    }
    // const quality = audioVideo.getVideoInputQualitySettings();
    // audioVideo.chooseVideoInputQuality(1280, 720, 15);
    // audioVideo.setVideoMaxBandwidthKbps(1400);
    // console.log("quality = ", quality);
    const attendeePresenceSet = new Set();
    const callback = (presentAttendeeId, present) => {
      if (present) {
        attendeePresenceSet.add(presentAttendeeId);
      } else {
        attendeePresenceSet.delete(presentAttendeeId);
      }
    };
    audioVideo.realtimeSubscribeToAttendeeIdPresence(callback);

    const localAttendeeId =
      meetingManager.meetingSession?.configuration.credentials?.attendeeId ||
      null;

    const observer = {
      remoteVideoSourcesDidChange: (videoSources) => {
        dispatch({
          type: VideoTileGridAction.UpdateVideoSources,
          payload: {
            videoSources,
            localAttendeeId,
          },
        });
      },
    };

    audioVideo.addObserver(observer);

    return () => audioVideo.removeObserver(observer);
  }, [audioVideo]);

  useEffect(() => {
    if (!audioVideo) {
      return;
    }

    const observer = {
      videoTileDidUpdate: (tileState) => {
        console.log("tilestate12378 = ", tileState);
        if (tileState.isContent) {
          const isSelfAttendee =
            new DefaultModality(tileState.boundAttendeeId).base() ===
            joinInfo?.Attendee?.AttendeeId;
          console.log("isSelfAttendee12378 = ", isSelfAttendee);
          if (isSelfAttendee) {
            return;
          }
        }
      }
    };

    audioVideo.addObserver(observer);

    return () => audioVideo.removeObserver(observer);
  }, [audioVideo, joinInfo]);

  useEffect(() => {
    if (!audioVideo || !priorityBasedPolicy) {
      return;
    }

    const observer = {
      tileWillBePausedByDownlinkPolicy: (tileId) => {
        const attendeeId = audioVideo.getVideoTile(tileId)?.state().boundAttendeeId;
        if (attendeeId) {
          dispatch({
            type: VideoTileGridAction.PauseVideoTile,
            payload: { attendeeId },
          });
        }
      },
      tileWillBeUnpausedByDownlinkPolicy: (tileId) => {
        const attendeeId = audioVideo.getVideoTile(tileId)?.state().boundAttendeeId;
        if (attendeeId) {
          dispatch({
            type: VideoTileGridAction.UnpauseVideoTile,
            payload: { attendeeId },
          });
        }
      },
    };

    priorityBasedPolicy.addObserver(observer);
    dispatch({
      type: VideoTileGridAction.SetPriorityBasedPolicy,
      payload: { policy: priorityBasedPolicy },
    });
    return () => priorityBasedPolicy.removeObserver(observer);
  }, [audioVideo]);

  useEffect(() => {
    const localAttendeeId =
      meetingManager.meetingSession?.configuration.credentials?.attendeeId ||
      null;

    dispatch({
      type: VideoTileGridAction.UpdateLocalSourceState,
      payload: {
        isVideoEnabled,
        localAttendeeId,
        isLocalUserSharing,
        sharingAttendeeId,
      },
    });
  }, [
    isLocalUserSharing,
    isVideoEnabled,
    meetingManager.meetingSession,
    sharingAttendeeId,
  ]);

  useEffect(() => {
    dispatch({
      type: VideoTileGridAction.UpdateLayout,
      payload: {
        layout,
      },
    });
  }, [layout]);

  useEffect(() => {
    if (sharingAttendeeId && layout === Layout.Gallery) {
      setLayout(Layout.Featured);
      dispatch({
        type: VideoTileGridAction.UpdateLayout,
        payload: {
          layout,
        },
      });
    }
  }, [sharingAttendeeId]);

  const zoomIn = () => dispatch({ type: VideoTileGridAction.ZoomIn });

  const zoomOut = () => dispatch({ type: VideoTileGridAction.ZoomOut });

  const controls = {
    zoomIn,
    zoomOut,
    layout,
    setLayout
  };

  return (
    <VideoTileGridStateContext.Provider value={state}>
      <VideoTileGridControlContext.Provider value={controls}>
        {children}
      </VideoTileGridControlContext.Provider>
    </VideoTileGridStateContext.Provider>
  );
};

const useVideoTileGridState = () => {
  const state = useContext(VideoTileGridStateContext);

  if (!state) {
    throw new Error(
      'useVideoTileGridState must be used within a VideoTileGridProvider'
    );
  }

  return state;
};
const useVideoTileGridControl = () => {
  const context = useContext(VideoTileGridControlContext);

  if (!context) {
    throw new Error(
      'useVideoTileGridControl must be used within VideoTileGridProvider'
    );
  }
  return context;
};
export { VideoTileGridProvider, useVideoTileGridState, useVideoTileGridControl };
