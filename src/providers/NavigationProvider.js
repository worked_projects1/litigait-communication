
import React, {
    useEffect,
} from 'react';
import { useLocation } from 'react-router-dom';
import { useMeetingManager } from 'amazon-chime-sdk-component-library-react';

import routes from '../constants/routes';

const NavigationProvider = ({ children }) => {

    const location = useLocation();
    const meetingManager = useMeetingManager();

    useEffect(() => {
        if (location.pathname.includes(routes.MEETING)) {
            return () => {
                meetingManager.leave();
            };
        }
    }, [location.pathname]);

    return (
        <>
            {children}
        </>
    );
};

export default NavigationProvider;
