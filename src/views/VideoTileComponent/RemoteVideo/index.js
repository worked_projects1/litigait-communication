
import React, { useEffect, useRef } from 'react';
import { useApplyVideoObjectFit, useAudioVideo, VideoTile } from 'amazon-chime-sdk-component-library-react';

export const RemoteVideo = ({
    name,
    className,
    tileId,
    ...rest
}) => {
    const audioVideo = useAudioVideo();
    const videoEl = useRef(null);
    useApplyVideoObjectFit(videoEl);

    useEffect(() => {
        if (!audioVideo || !videoEl.current) {
            return;
        }

        audioVideo.bindVideoElement(tileId, videoEl.current);

        return () => {
            const tile = audioVideo.getVideoTile(tileId);
            if (tile) {
                audioVideo.unbindVideoElement(tileId);
            }
        };
    }, [audioVideo, tileId]);

    return (
        <VideoTile
            {...rest}
            ref={videoEl}
            nameplate={name}
            className={`ch-remote-video--${tileId} ${className || ''}`}
        />
    );
};

export default RemoteVideo;
