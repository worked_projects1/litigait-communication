
import React, { useEffect, useRef } from 'react';
import { useAudioVideo, useContentShareState, VideoTile } from 'amazon-chime-sdk-component-library-react';
import styled from 'styled-components';

const ContentTile = styled(VideoTile)`
  background-color: ${({ theme }) => theme.colors.greys.grey80};
`;

export const ContentShare = ({ className, ...rest }) => {
    const audioVideo = useAudioVideo();
    const { tileId } = useContentShareState();
    const videoEl = useRef(null);

    useEffect(() => {
        if (!audioVideo || !videoEl.current || !tileId) {
            return;
        }
        console.log("tileId3432 = ", tileId);
        audioVideo.bindVideoElement(tileId, videoEl.current);

        return () => {
            const tile = audioVideo.getVideoTile(tileId);
            if (tile) {
                audioVideo.unbindVideoElement(tileId);
            }
        };
    }, [audioVideo, tileId]);

    return tileId ? (
        <ContentTile
            objectFit="contain"
            className={className || ''}
            {...rest}
            ref={videoEl}
        />
    ) : null;
};

export default ContentShare;
