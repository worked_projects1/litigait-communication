
import React, { useEffect, useRef, forwardRef, useState } from 'react';
import { useAudioVideo, useContentShareState, useContentShareControls } from 'amazon-chime-sdk-component-library-react';
import styled from 'styled-components';
import { Button, Grid, Typography } from '@mui/material';
import { StyledVideoTile } from './Styled';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import StopIcon from '@mui/icons-material/Stop';

// const ContentTile = styled(VideoTile)`
//   background-color: ${({ theme }) => theme.colors.greys.grey80};
// `;

const VideoTile = forwardRef(
    (props, ref) => {
        const { tag, className, nameplate, ...rest } = props;

        return (
            <StyledVideoTile
                name="localsharetile"
                as={tag}
                className={className || ''}
                data-testid="video-tile"
                {...rest}
            >
                <video ref={ref} className="ch-video" />
                {nameplate && (
                    <header className="ch-nameplate">
                        <p className="ch-text">{nameplate}</p>
                    </header>
                )}
            </StyledVideoTile>
        );
    }
);


export const LocalContentShare = ({ className, ...rest }) => {
    const audioVideo = useAudioVideo();
    const { tileId } = useContentShareState();
    const {
        paused,
        toggleContentShare,
        togglePauseContentShare,
        isLocalShareLoading
    } =
        useContentShareControls();
    const videoEl = useRef(null);
    const [show, setShow] = useState(true);
    const [arrowShow, setArrowShow] = useState(true);
    const [isClickedOutside, setIsClickedOutside] = useState(false);
    const elementRef = useRef(null);

    useEffect(() => {
        const initialShow = () => {
            setTimeout(() => {
                setShow(false);
                setArrowShow(false);
            }, 3000);
        }
        initialShow();
        // return () => setShow(true);
    }, []);

    // useEffect(() => {
    //     if (isClickedOutside) {
    //         setArrowShow(true);
    //     }
    // }, [isClickedOutside]);

    useEffect(() => {
        const handleClickOutside = (e) => {
            if (elementRef.current && !elementRef.current.contains(e.target)) {
                console.log("outside  = ", !show)
                // The click occurred outside the element.
                // setIsClickedOutside(true);
                if (!show) {
                    setArrowShow(false);
                }
                // else {
                //     setArrowShow(true);
                // }
                // setArrowShow(true);
            } else {
                console.log("inside ")
                // The click occurred inside the element.
                // setIsClickedOutside(false);
                setArrowShow(true);
                // setArrowShow(false);
            }
        };

        document.addEventListener("click", handleClickOutside);

        return () => {
            document.removeEventListener("click", handleClickOutside);
        };
    }, [elementRef, show]);

    useEffect(() => {
        if (!audioVideo || !videoEl.current || !tileId) {
            return;
        }
        console.log("tileId3432 = ", tileId);
        audioVideo.bindVideoElement(tileId, videoEl.current);

        return () => {
            const tile = audioVideo.getVideoTile(tileId);
            if (tile) {
                audioVideo.unbindVideoElement(tileId);
            }
        };
    }, [audioVideo, tileId, show]);
    console.log("show34r4 = ", show, "videoE; - ", videoEl, "tileId = ", tileId);
    return tileId ? (
        <Grid style={{
            position: 'fixed',
            top: 0,
            zIndex: "1500",
            left: '50%',
            height: '35px'
        }}
            container
            name="localcontentshare"

        >
            <Grid item
                style={{
                    height: '100%'
                }}
            >
                <Grid
                    container
                    style={{
                        padding: '7px',
                        width: '250px',
                        height: '100%',
                        flexDirection: 'column',
                        backgroundColor: '#2ca01c',
                        position: 'relative',
                        borderRadius: '4px 4px 0px 0px'
                    }}
                    ref={elementRef}
                    onMouseEnter={() => !show && setArrowShow(true)}
                    onMouseLeave={() => !show && setArrowShow(false)}
                >
                    <Grid
                        style={{
                            height: '100%'
                        }}

                    >
                        <Typography
                            variant='subtitle2'
                            sx={{
                                fontFamily: 'Avenir-Bold',
                                textAlign: 'center'
                            }}
                        >
                            You're screen sharing
                        </Typography>
                    </Grid>
                    <Grid
                        style={{
                            position: 'absolute',
                            top: '35px',
                            minHeight: show ? '150px' : '20px',
                            width: '100%',
                            left: '0'

                        }}
                    >
                        {show ? <Grid
                            style={{
                                width: '100%',
                                height: '145px',
                                padding: '5px',
                                backgroundColor: '#2ca01c'
                            }}
                        >
                            <VideoTile
                                objectFit="contain"
                                className={className || ''}
                                {...rest}
                                ref={videoEl}
                            />
                        </Grid> : null}
                        {arrowShow ?
                            <Grid
                                style={{
                                    // height: "15%",
                                    // minHeight: '20px'
                                }}>
                                <Button
                                    sx={{
                                        width: '100%',
                                        // backgroundColor: '#2ca01c',
                                        color: '#000',
                                        "&:hover": {
                                            backgroundColor: '#2ca01cd9 !important',
                                        },
                                        height: '20px',
                                        borderRadius: '0px 0px 4px 4px'
                                    }}
                                    onClick={() => {
                                        setShow(!show);
                                        setArrowShow(true);
                                    }}
                                >
                                    {!show ? <ArrowDropDownIcon /> : <ArrowDropUpIcon />}
                                </Button>
                            </Grid>
                            : null}
                    </Grid>
                </Grid>
            </Grid>
            <Grid item
                style={{
                    height: '100%'
                }}
            >
                <Button
                    style={{
                        width: '100%',
                        backgroundColor: 'red',
                        color: 'white',
                        padding: '5px 8px'
                        // height: '35px'
                    }}
                    startIcon={<StopIcon
                        fontSize='small'
                        sx={{
                            color: 'white'
                        }} />}
                    onClick={() => {
                        // const tile = audioVideo.getVideoTile(tileId);
                        // if (tile) {
                        //     audioVideo.unbindVideoElement(tileId);
                        // }
                        toggleContentShare()
                    }}
                >
                    Stop
                </Button>
            </Grid>
        </Grid>
    ) : null;
};

