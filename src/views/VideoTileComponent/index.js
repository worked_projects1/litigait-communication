
import React, { useState, useEffect, useRef } from 'react';
import { Layout } from '../../types';
import { FeaturedRemoteVideos } from './FeaturedRemoteVideos';
import { RemoteVideos } from './RemoteVideos';
import {
    useContentShareState,
    VideoGrid,
    BackgroundBlurProvider,
    BackgroundReplacementProvider,
    useMeetingManager,
    useVideoInputs,
    Severity,
    ActionType,
    useNotificationDispatch,
    useRosterState,
    useNotificationState
} from 'amazon-chime-sdk-component-library-react';
import {
    isVideoTransformDevice,
} from 'amazon-chime-sdk-js';
import MeetingControlsTotal from '../../containers/MeetingControlTotal';
import NavigationControl from '../../containers/Navigation';
import { useAppState } from '../../providers/AppStateProvider';
import { useVideoTileGridControl } from '../../providers/VideoTileGridProvider';
import { Grid, Snackbar } from '@mui/material';
import RecordIndicator from './RecordIndicaor';
// import TranscriptEvent from './TranscriptEvent';
import { endrecordingVideo } from '../../utils/api';
import styled from 'styled-components';
import useStyles from './styles';
import { LocalContentShare } from './LocalContentShare';

const isDesktop = () => window.innerWidth > 768;

export const CustomisedSnackbar = styled(Snackbar)`
bottom: 15%  !important;
& .MuiSnackbarContent-root {
    text-align: center;
    width: 100%;
    min-width: auto;
}
@media screen and (max-width: 768px) {
    bottom: 10% !important;
}
`;

/**
 * Hook that alerts clicks outside of the passed ref
 */
// function useOutsideAlerter(ref, setShowOutside) {
//     useEffect(() => {
//         /**
//          * Alert if clicked on outside of element
//          */
//         function handleClickOutside(event) {
//             if (ref.current && !ref.current.contains(event.target)) {
//                 //   alert("You clicked outside of me!");
//                 setShowOutside(true);
//             } else {
//                 setShowOutside(false);
//             }
//         }
//         // Bind the event listener
//         document.addEventListener("mousedown", handleClickOutside);
//         return () => {
//             // Unbind the event listener on clean up
//             document.removeEventListener("mousedown", handleClickOutside);
//         };
//     }, [ref]);
// }

export const VideoTileComponent = () => {
    // const { layout, showRoster, setShowRoster, showChat, setShowChat } = useAppState();
    // const { imageBlob: defaultImage } = useAppState();
    const { meetingId, joinInfo } = useAppState();
    const { roster } = useRosterState();
    const dispatch = useNotificationDispatch();
    const meetingManager = useMeetingManager();
    const { tileId: contentTileId, sharingAttendeeId, isLocalUserSharing } = useContentShareState();
    const { layout } = useVideoTileGridControl();
    const { selectedDevice } = useVideoInputs();
    const [showRoster, setShowRoster] = useState(false);
    const [showChat, setShowChat] = useState(false);
    const [showBackgroundChange, setShowBackgroundChange] = useState(false);
    const isDesktopView = useRef(isDesktop());
    const [transcriptionTotal, setTranscriptionTotal] = useState([]);
    const [startTranscriptFun, setStartTranscriptFun] = useState(false);
    const [capturePipeline, setMediaCapturePipeline] = useState({});
    const [showPopup, setShowPopup] = useState(false);
    const [imageBlob, setImageBlob] = useState({});
    const [showNotofication, setShowNotofication] = useState(false);
    const [showOutside, setShowOutside] = useState(false);
    let attendees = Object.values(roster);
    let filterCPUUtilization = 20;
    const { notifications } = useNotificationState();
    const { classes } = useStyles();
    // const wrapperRef = useRef(null);
    // useOutsideAlerter(wrapperRef, setShowOutside);

    console.log("notification = 435", notifications);
    useEffect(() => {
        resetDeviceToIntrinsic();
    }, []);

    // Reset the video input to intrinsic if current video input is a transform device because this component
    // does not know if blur or replacement was selected. This depends on how the demo is set up.
    // TODO: use a hook in the appState to track whether blur or replacement was selected before this component mounts,
    // or maintain the state of `activeVideoTransformOption` in `MeetingManager`.
    const resetDeviceToIntrinsic = async () => {
        try {
            if (isVideoTransformDevice(selectedDevice)) {
                const intrinsicDevice = await selectedDevice.intrinsicDevice();
                await meetingManager.selectVideoInputDevice(intrinsicDevice);
            }
        } catch (error) {
            console.log('Failed to reset Device to intrinsic device');
        }
    };
    // console.log("meetingmanager = ", meetingManager, "joinInfo = ", joinInfo);
    // useEffect(() => {
    //     const myFunction = (e) => {
    //         // Display a custom confirmation dialog
    //         const confirmationMessage = 'Text message?';

    //         // Use the confirm method to show your custom dialog
    //         // if (!window.confirm(confirmationMessage)) {
    //         console.log("sss = ");
    //         stopRecording();
    //         meetingManager.leave();
    //         e.preventDefault();
    //         e.stopPropagation();
    //         e.returnValue = confirmationMessage;
    //         // }
    //         return "Leaving this page will reset the wizard";
    //     }
    //     // if (showPopup) {
    //     window.addEventListener("beforeunload", myFunction);
    //     // }

    //     return () => window.removeEventListener("beforeunload", myFunction);
    // }, [showPopup]);

    useEffect(() => {
        const handler = () => {
            const isResizeDesktop = isDesktop();
            if (isDesktopView.current === isResizeDesktop) {
                return;
            }

            isDesktopView.current = isResizeDesktop;

            if (!isResizeDesktop) {
                setShowRoster(false);
                setShowChat(false);
            }
        };

        window.addEventListener('resize', handler);
        return () => {
            window.removeEventListener('resize', handler);
        }
    }, []);

    // useEffect(() => {

    //     if (attendees.length == 1) {
    //         setShowNotofication(true);
    //         // const payload = {
    //         //     severity: Severity.INFO,
    //         //     message: "Please wait for attorney to join",
    //         //     autoClose: false,
    //         // };
    //         // dispatch({
    //         //     type: ActionType.ADD,
    //         //     payload: payload,
    //         // });
    //     } else {
    //         // const payload = {
    //         //     severity: Severity.INFO,
    //         //     message: "Please wait for attorney to join",
    //         //     autoClose: false,
    //         // };
    //         // dispatch({
    //         //     type: ActionType.REMOVE,
    //         //     payload: payload,
    //         // });
    //         setShowNotofication(false);
    //     }
    // }, [roster]);

    // const stopRecording = async () => {
    //     try {
    //         if (meetingId) {
    //             await endrecordingVideo(meetingId);
    //         }
    //     } catch (error) {
    //         const payload = {
    //             severity: Severity.ERROR,
    //             message: (error).message,
    //             autoClose: true,
    //         };
    //         dispatch({
    //             type: ActionType.ADD,
    //             payload: payload,
    //         });
    //     }
    // };

    const toggleRoster = () => {
        setShowRoster(!showRoster);
        setShowChat(false);
        setShowBackgroundChange(false);
    };

    const openRoster = () => {
        setShowRoster(true);
    };

    const closeRoster = () => {
        setShowRoster(false);
    };

    const toggleChat = () => {
        setShowChat(!showChat);
        setShowRoster(false);
        setShowBackgroundChange(false);
    };

    const toggleBackgroundChange = () => {
        setShowBackgroundChange(!showBackgroundChange);
        setShowRoster(false);
        setShowChat(false);
    };

    return (
        <BackgroundBlurProvider options={{ filterCPUUtilization }} >
            <BackgroundReplacementProvider options={{ imageBlob, filterCPUUtilization }} >
                <Grid
                    // className='backgroundContainer'
                    // ref={wrapperRef}
                    className={classes.backgroundContainer}
                >
                    <Grid
                        // className='totalScreenContainer'
                        className={classes.totalScreenContainer}
                    >
                        <RecordIndicator
                            setShowPopup={setShowPopup}
                        />
                        {/* <LocalContentShare /> */}
                        {sharingAttendeeId && isLocalUserSharing ? <LocalContentShare /> : null}
                        {/* uncommend below component when you need to show transcript text */}
                        {/* <TranscriptEvent /> */}
                        <Grid
                            // className='totalTileContainer'
                            className={classes.totalTileContainer}
                        >
                            <VideoGrid
                                layout={layout === Layout.Gallery ? 'standard' : 'featured'}
                                // className='videoGrid'
                                className={classes.videoGrid}
                            >
                                <Grid
                                    style={{ width: "100%", height: "100%", display: 'flex' }}
                                >
                                    {layout === Layout.Gallery && !contentTileId ? <RemoteVideos /> : <FeaturedRemoteVideos />}
                                </Grid>
                            </VideoGrid>
                            {<MeetingControlsTotal
                                navigationControl={Object.assign({}, {
                                    showRoster: showRoster,
                                    toggleRoster: toggleRoster,
                                    openRoster: openRoster,
                                    closeRoster: closeRoster,
                                    showChat: showChat,
                                    toggleChat: toggleChat,
                                    setShowChat: setShowChat,
                                    setMediaCapturePipeline: setMediaCapturePipeline,
                                    capturePipeline: capturePipeline,
                                    transcriptionTotal: transcriptionTotal,
                                    setTranscriptionTotal: setTranscriptionTotal,
                                    startTranscriptFun: startTranscriptFun,
                                    setStartTranscriptFun: setStartTranscriptFun,
                                    toggleBackgroundChange: toggleBackgroundChange,
                                    showBackgroundChange: showBackgroundChange
                                })}
                            />}
                        </Grid>
                        <NavigationControl
                            navigationControl={Object.assign({}, {
                                showRoster: showRoster,
                                toggleRoster: toggleRoster,
                                openRoster: openRoster,
                                closeRoster: closeRoster,
                                showChat: showChat,
                                toggleChat: toggleChat,
                                setShowChat: setShowChat,
                                toggleBackgroundChange: toggleBackgroundChange,
                                showBackgroundChange: showBackgroundChange,
                                setImageBlob: setImageBlob,
                                imageBlob: imageBlob
                            })}
                        />
                        {/* <CustomisedSnackbar
                            open={showNotofication}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center'
                            }}
                            // sx={{
                            //     bottom: "10%",
                            //     "& .MuiSnackbar-root": {
                            //         bottom: "15% !important",
                            //     },
                            //     '& .MuiSnackbarContent-root': {
                            //         textAlign: 'center',
                            //         width: '100%',
                            //         minWidth: 'auto'
                            //     }
                            // }}
                            message={"Please wait for attorney to join"}
                        >
                        </CustomisedSnackbar> */}
                    </Grid>
                </Grid>
            </BackgroundReplacementProvider>
        </BackgroundBlurProvider>
    );
};

export default VideoTileComponent;
