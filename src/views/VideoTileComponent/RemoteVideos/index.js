
import React, { memo } from 'react';
import { useRemoteVideoTileState, useRosterState, Flex, useLocalVideo } from 'amazon-chime-sdk-component-library-react';
import { RemoteVideo } from '../RemoteVideo';
import { LocalVideo } from '../LocalVideo';
import styled from 'styled-components';
import { Grid } from '@mui/material';

export const RemoteVideos = (props) => {
    const { roster } = useRosterState();
    const { tiles, tileIdToAttendeeId } = useRemoteVideoTileState();
    const { isVideoEnabled } = useLocalVideo();
    const isDesktop = () => window.innerWidth > 768;

    const calculateSize = () => {
        if (isDesktop()) {
            if (tiles.length > 2 || (tiles.length == 1 && isVideoEnabled) || (tiles.length == 2)) {
                return '49%';
            } else if ((tiles.length < 1 && isVideoEnabled) || (tiles.length == 1 && !isVideoEnabled)) {
                return '100%';
            }
        } else {
            if (tiles.length > 2) {
                return '49%';
            } else if ((tiles.length < 1 && isVideoEnabled) || (tiles.length == 1 && !isVideoEnabled) || (tiles.length == 1 && isVideoEnabled)) {
                return '100%';
            } else {
                return '100%';
            }
        }
    }

    const CustomisedFlex = styled(Grid)`
        position: relative;
        width: ${props => calculateSize()};
        max-height: ${props => (tiles.length > 2 ? '50%' : (tiles.length == 1 || isVideoEnabled) && isDesktop() ? '100%' : '50%')};
        margin: 2px;
    `;

    return (
        <Grid layout="equal-columns" container flexWrap="wrap" justifyContent="space-evenly" name="totalContainer"
            style={{ width: "100%", height: "100%" }}
        >
            {isVideoEnabled ?
                <CustomisedFlex>
                    <LocalVideo nameplate={"me"} />
                </CustomisedFlex>
                : null}

            {tiles.map((tileId, i) => {

                if (tileIdToAttendeeId[tileId]) {
                    const attendee = roster[tileIdToAttendeeId[tileId]] || {};
                    const { name } = attendee;
                    return (<CustomisedFlex>
                        <RemoteVideo {...props} key={tileId} tileId={tileId} name={name} className="gridViewRemote" />
                    </CustomisedFlex>
                    );
                }
            })}
        </Grid>
    );
};

export default memo(RemoteVideos);
