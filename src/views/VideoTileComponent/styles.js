import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    backgroundContainer: {
        width: "100%",
        display: "flex",
        backgroundColor: "rgb(152 151 151 / 25%)"
    },
    totalScreenContainer: {
        width: "100%",
        height: "100%",
        position: "relative",
        display: "flex"

    },
    totalTileContainer: {
        width: "100%",
        height: "100%",
        position: "relative"
    },
    videoGrid: {
        borderRadius: "5px",
        background: "transparent",
        boxShadow: "0px 0px 8px 1px lightgrey",
        height: "100%",
        display: "flex"

    }
}));

export default useStyles;