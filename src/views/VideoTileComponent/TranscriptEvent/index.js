
import React, { useEffect, useState } from "react";
import { useAudioVideo, useRosterState } from "amazon-chime-sdk-component-library-react";
import { Grid } from "@mui/material";
import useStyles from "./styles";

const TranscriptEvent = () => {
    const [transcripts, setTranscripts] = useState([]);
    const [lines, setLine] = useState([]);
    const audioVideo = useAudioVideo();
    const { roster } = useRosterState();
    const { classes } = useStyles();
    let attendees = Object.values(roster);
    useEffect(() => {
        if (!audioVideo) {
            return;
        }
        if (audioVideo) {
            audioVideo.transcriptionController?.subscribeToTranscriptEvent((transcriptEvent) => {
                setTranscripts(transcriptEvent);
            });
        }
    }, [audioVideo]);

    useEffect(() => {
        if (transcripts) {
            if (transcripts.results !== undefined) {
                if (!transcripts.results[0].isPartial) {
                    const attendeeItem = attendees.find((attendee) => transcripts.results[0].alternatives[0].items[0].attendee.attendeeId == attendee.chimeAttendeeId);
                    const attendeeName = attendeeItem?.name;
                    // setLine((lines) => [
                    //     ...lines,
                    //     `${attendeeName}: ${transcripts.results[0].alternatives[0].transcript}`,
                    // ]);

                    setTimeout(() => setLine(''), 1000);
                    var obj = {};
                    obj[attendeeName] = transcripts.results[0].alternatives[0].transcript;

                } else {
                    const attendeeItem = attendees.find((attendee) => transcripts.results[0].alternatives[0].items[0].attendee.attendeeId == attendee.chimeAttendeeId);
                    const attendeeName = attendeeItem?.name;
                    setLine(
                        `${attendeeName}: ${transcripts.results[0].alternatives[0].transcript}`,
                    );
                    // setTimeout(() => setLine(''), 1000);
                }
            }
        }
    }, [transcripts]);

    return (<Grid
        container
        // className="transcriptGrid"
        className={classes.transcriptGrid}
    >
        <span>{lines}</span>
    </Grid>)

}

export default TranscriptEvent;