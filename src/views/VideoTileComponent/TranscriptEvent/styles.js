import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    transcriptGrid: {
        position: "absolute",
        top: "50%",
        backgroundColor: "transparent !important",
        justifyContent: "center",
        zIndex: "1"
    }
})
);

export default useStyles;