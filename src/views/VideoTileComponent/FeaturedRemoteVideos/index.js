
import React, { memo, useState, useMemo, useEffect } from 'react';
import { RemoteVideo } from '../RemoteVideo';
import { LocalVideo } from '../LocalVideo';
import { ContentShare } from '../ContentShare';
import { useContentShareState, useFeaturedTileState, useRemoteVideoTileState, useRosterState, useLocalVideo, Flex, useActiveSpeakersState } from 'amazon-chime-sdk-component-library-react';
import { Box, Grid, useMediaQuery } from '@mui/material';
import { createTheme } from '@mui/system';

export const FeaturedRemoteVideos = (props) => {
    const { roster } = useRosterState();
    const { tileId: featuredTileId } = useFeaturedTileState();
    const { tileId: contentTileId, sharingAttendeeId, isLocalUserSharing } = useContentShareState();
    const { tileId: localTileId, isVideoEnabled } = useLocalVideo();
    const { tiles, tileIdToAttendeeId } = useRemoteVideoTileState();
    const activeSpeakers = useActiveSpeakersState();
    const [bigTile, setBigTile] = useState([]);
    const [otherTile, setOtherTile] = useState([]);
    const totalRosterAttendees = Object.values(roster);
    const theme = createTheme();

    const tileMap = () => {
        return tiles.map(tileId => {
            if (Object.keys(roster).length && tileIdToAttendeeId[tileId]) {
                const obj = roster[tileIdToAttendeeId[tileId]];
                return Object.assign({}, { attendeeId: obj?.chimeAttendeeId, externalUserId: obj?.externalUserId, name: obj.name, tileId: tileId, speaker: activeSpeakers.includes(obj?.chimeAttendeeId) ? activeSpeakers.find(e => e == obj?.chimeAttendeeId) : "" })
            } else {
                return {}
            }
        })
    }

    const changeBigTile = useMemo(() => {
        // const rosterAttendees = totalRosterAttendees.map((e) => Object.assign({}, { attendeeId: e?.chimeAttendeeId, externalUserId: e?.externalUserId, name: e?.name }));
        if ((tiles.length !== 0 && !sharingAttendeeId) || (tiles.length !== 0 && sharingAttendeeId && isLocalUserSharing)) {
            return {
                bigTile: tileMap().filter((e, i) => i === 0)
                ,
                otherTile: tileMap().filter((e, i) => i !== 0),
            };
        } else {
            return {
                bigTile: [],
                otherTile: tileMap(),
            };
        }
    }, [tiles, sharingAttendeeId, activeSpeakers]);
    console.log("condition324 = ", tiles.length !== 0 && sharingAttendeeId && !isLocalUserSharing);
    useEffect(() => {
        setBigTile(changeBigTile.bigTile);
        setOtherTile(changeBigTile.otherTile);
    }, [changeBigTile, tiles, sharingAttendeeId, activeSpeakers]);


    return (<Grid style={{ width: '100%', position: "relative" }} container name="FeaturedContainer">
        <Grid style={{ width: '100%' }}>
            {sharingAttendeeId && !isLocalUserSharing ? <Grid
                name="Remote Video"
                className='screeanShareFeaturedContainer'
            >
                <ContentShare />
            </Grid> : tiles.length != 0 ? (bigTile || []).map((e, i) => {
                const featured = !contentTileId && featuredTileId === e.tileId;
                const styles = featured ? '' : '';
                const classes = `${featured ? '' : ''} ${props.className || ''
                    }`;
                if (tileIdToAttendeeId[e.tileId]) {
                    const attendee = roster[tileIdToAttendeeId[e.tileId]] || {};
                    const { name } = attendee;
                    return (<Grid
                        name="Remote Video"
                        className='FeaturedFirstRemoteContaniner'
                        style={{ border: e.speaker ? "1px solid #e95d0c" : "none" }}
                    >

                        <RemoteVideo
                            tileId={e.tileId}
                            name={name}
                            {...props}
                            className={classes}
                            key={e.tileId}
                            css={styles}
                        />
                    </Grid>);
                }
            }) : null}
        </Grid>
        {/* <Grid className='featuredRightViewContainer' style={{ display: "flex" }}> */}
        <Box
            className='featuredRightViewContainer'
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                // p: 1,
                // m: 1,
                bgcolor: 'background.paper',
                // borderRadius: 1,
            }}
        >
            {isVideoEnabled ?
                <>
                    {/* <Grid className="featuredLocalVideoFlex2">

                    </Grid> */}

                    {/* <Grid item xs={2} name="localVideoFlex" className='featureLocalVideoFlex'> */}
                    <Box name="localVideoFlex" className='featureLocalVideoFlex'>
                        <Box className="featureLocaleInsideFlex">
                            <LocalVideo nameplate="me" className="featuredLocalVideo" />
                        </Box>
                    </Box>
                    {/* </Grid> */}
                </> : null}

            {tiles.length != 0 ? otherTile.map((e, i) => {
                const featured = !contentTileId && featuredTileId === e.tileId;
                const styles = featured ? '' : '';
                const classes = `${featured ? '' : ''} ${props.className || ''
                    }`;
                if (tileIdToAttendeeId[e.tileId]) {
                    const attendee = roster[tileIdToAttendeeId[e.tileId]] || {};
                    const { name } = attendee;
                    return (<>
                        {/* <Grid className="featuredRemoteContainer2">

                        </Grid> */}
                        {/* <Grid
                            item xs={2}
                            name="Remote Video"
                            className="featuredRemoteContainer"
                            style={{ border: e.speaker ? "1px solid #e95d0c" : "none" }}
                        > */}
                        <Box name="Remote Video"
                            className="featuredRemoteVideoFlex">
                            <Box className="featureRemoteInsideFlex">
                                <RemoteVideo
                                    tileId={e.tileId}
                                    name={name}
                                    {...props}
                                    className={"featuredRemoteVideo"}
                                    key={e.tileId}
                                    css={styles}
                                />
                            </Box>
                        </Box>

                        {/* // </Grid> */}
                    </>);
                }
            })
                : null}
        </Box>
        {/* </Grid> */}
    </Grid>
    )
};

export default memo(FeaturedRemoteVideos);
