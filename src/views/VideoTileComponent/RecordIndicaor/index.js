
import React, { useState, useEffect } from 'react';
import { endrecordingVideo } from '../../../utils/api';
import { Grid, Button } from '@mui/material';
import {
    useRosterState, Severity,
    ActionType,
    useNotificationDispatch
} from 'amazon-chime-sdk-component-library-react';
import { useAppState } from '../../../providers/AppStateProvider';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';

const RecordIndicator = (props) => {
    const { setShowPopup } = props;
    const { roster } = useRosterState();
    const { meetingId } = useAppState();
    const [showRecordIndicator, setShowRecordIndicator] = useState(false);
    const [count, setCount] = useState(0);
    const dispatch = useNotificationDispatch();

    useEffect(() => {
        let attendees = Object.values(roster);
        const mediaPipeLine = attendees.find((attendee) =>
            attendee?.name?.toLowerCase().includes("recording")
        );
        let timerInterval = null;
        if (mediaPipeLine && mediaPipeLine != -1) {
            setShowRecordIndicator(true);
            setShowPopup(true);
            timerInterval = setInterval(() => {
                setCount((prevSeconds) => prevSeconds + 1);
            }, 1000);
        } else {
            setShowRecordIndicator(false);
            setShowPopup(false);
            setCount(0);
            clearInterval(timerInterval);
        }
    }, [roster]);


    const dispSecondsAsMins = (seconds) => {
        const mins = Math.floor(seconds / 60);
        const seconds_ = seconds % 60;
        return mins.toString() + ":" + (seconds_ == 0 ? "00" : seconds_.toString());
    };

    const stopRecording = async () => {
        try {
            if (meetingId) {
                await endrecordingVideo(meetingId);
            }
        } catch (error) {
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }
    };

    return (
        showRecordIndicator ?
            <Grid
                style={{ position: "fixed", top: '0px', left: "0px", right: '0px', background: 'transparent  !important', justifyContent: 'space-between', alignItems: 'center', zIndex: 1, filter: 'drop-shadow(2px 4px 6px black)', backgroundColor: 'rgb(152 151 151 / 25%)' }}
                container
            >
                <Grid style={{ padding: '10px', background: 'transparent  !important' }} className="recordindicateBtnGrid">
                    <Button
                        // onClick={stopRecording}
                        size="small"
                        startIcon={<RadioButtonCheckedIcon fontSize="medium" style={{ color: 'red' }} />}
                        sx={{ background: 'transparent  !important' }}
                        disabled
                    >
                        <span style={{ lineHeight: 1, color: '#e95d0c' }}>{dispSecondsAsMins(count)}</span>
                    </Button>
                </Grid>

                <Grid style={{ padding: '10px', background: 'transparent  !important' }} className="recordindicateBtnGrid">
                    <Button
                        onClick={stopRecording}
                        size="small"
                        sx={{ background: '#838384  !important', lineHeight: 1, border: '1px solid #e95d0c', color: '#fff' }}
                    >
                        Stop Recording
                    </Button>
                </Grid>
            </Grid>
            : null
    )

}

export default RecordIndicator;