
import React from 'react';

import MeetingFormSelector from '../../containers/MeetingFormSelector';
import { StyledLayout } from './Styled';

const Home = () => (
  <StyledLayout>
    <MeetingFormSelector />
  </StyledLayout>
);

export default Home;
