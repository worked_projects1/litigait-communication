
import styled from 'styled-components';

export const StyledLayout = styled.main`
  display: flex;
  min-height: 100%;
  margin: auto;
  align-items: center;
  justify-content: center;
  
  @media (min-width: 600px) and (min-height: 600px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 2rem;
  }
`;
