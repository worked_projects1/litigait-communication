
import React from 'react';
import useMeetingEndRedirect from '../../hooks/useMeetingEndRedirect';
import { VideoTileGridProvider } from '../../providers/VideoTileGridProvider';
import { DataMessagesProvider } from '../../providers/DataMessagesProvider';
import MeetingStatusNotifier from '../../containers/MeetingStatusNotifier';
import styled from 'styled-components';
import './styles.css';
import VideoTileComponent from '../VideoTileComponent';

const MeetingView = () => {
  useMeetingEndRedirect();

  const StyledLayout = styled.main`
  height: 100%;
  width: 100%;
  display: flex;
`;

  return (
    <DataMessagesProvider>
      <VideoTileGridProvider>
        <StyledLayout>
          <VideoTileComponent />
          {/* <MeetingStatusNotifier /> */}
        </StyledLayout>
      </VideoTileGridProvider>
    </DataMessagesProvider>
  );
};

export default MeetingView;
