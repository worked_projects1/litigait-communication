import { lightTheme, darkTheme } from 'amazon-chime-sdk-component-library-react';

const { colors: lightThemeColors, shadows: lightThemeShadows } = lightTheme;
const { colors: darkThemeColors, shadows: darkThemeShadows } = darkTheme;

const chatLightTheme = {
  title: lightThemeColors.greys.grey100,
  primaryText: lightThemeColors.greys.grey80,
  secondaryText: lightThemeColors.greys.grey50,
  headerBorder: lightThemeColors.greys.grey40,
  containerBorder: lightThemeColors.greys.grey30,
  bgd: lightThemeColors.greys.grey10,
  fgd: lightThemeColors.greys.white,
  shadow: lightThemeShadows.large,
  maxWidth: '18.5rem',
};

const chatDarkTheme = {
  title: darkThemeColors.greys.white,
  primaryText: darkThemeColors.greys.white,
  secondaryText: darkThemeColors.greys.grey20,
  headerBorder: darkThemeColors.greys.grey40,
  containerBorder: darkThemeColors.greys.grey30,
  bgd: darkThemeColors.greys.grey100,
  fgd: darkThemeColors.greys.grey60,
  shadow: darkThemeShadows.large,
  maxWidth: '18.5rem',
};

lightTheme.buttons.icon.active = Object.assign({}, lightTheme.buttons.icon.active, {
  bgd: "#e95d0c",
  text: "#fff",
  border: '1px solid #e95d0c',
  width: "40px",
  height: "40px",
  shadow: "none"
});

lightTheme.buttons.icon.focus = Object.assign({}, lightTheme.buttons.icon.focus, {
  bgd: "#e95d0c",
  text: "#fff",
  border: '1px solid #e95d0c',
  width: "35px",
  height: "35px",
  shadow: "none",
  "&: before": {
    boxShadow: "inset 0 0 0 5em rgba(255,255,255,0.2)",
    transition: "box-shadow 0.8s"
  }
});

lightTheme.buttons.icon.hover = Object.assign({}, lightTheme.buttons.icon.hover, {
  bgd: "#e95d0c",
  text: "#fff",
  border: '1px solid #fff',
  width: "45px",
  height: "45px",
  shadow: "0px 0px 8px 1px #e95d0c"
});

lightTheme.buttons.icon.static = Object.assign({}, lightTheme.buttons.icon.static, {
  bgd: "transparent",
  text: "#e95d0c",
  border: '1px solid #e95d0c',
  width: "40px",
  height: "40px",
  shadow: "none"
});

lightTheme.popOver = Object.assign({}, lightTheme.popOver, {
  itemText: "#000",
  menuBgd: "#fff",
  menuBorder: 'none',
  separator: "#fff",
  titleText: "#000",
  shadow: "0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0.14), 0px 3px 14px 2px rgba(0,0,0,0.12);",
  active: {
    itemText: "#000",
    itemBgd: "rgba(0, 0, 0, 0.08)"
  }
});

lightTheme.fonts.body = "'Avenir-Regular', 'Avenir-Bold', 'Avenir-Heavy', 'Avenir-Italic'"

export const demoLightTheme = {
  ...lightTheme,
  chat: chatLightTheme,
};

export const demoDarkTheme = {
  ...darkTheme,
  chat: chatDarkTheme,
};
