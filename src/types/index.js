
export var MeetingMode;

(function (MeetingMode) {
  MeetingMode[MeetingMode["Spectator"] = 0] = "Spectator";
  MeetingMode[MeetingMode["Attendee"] = 1] = "Attendee";
})(MeetingMode || (MeetingMode = {}));

export var Layout;

(function (Layout) {
  Layout[Layout["Gallery"] = 0] = "Gallery";
  Layout[Layout["Featured"] = 1] = "Featured";
})(Layout || (Layout = {}));

// Different CPU Utilizations percentage options for initializing background blur and replacement processors
export const VideoFiltersCpuUtilization = {
  Disabled: '0',
  CPU10Percent: '10',
  CPU20Percent: '20',
  CPU40Percent: '40',
};

// Video Transform Options
export const VideoTransformOptions = {
  None: 'None',
  Blur: 'Background Blur',
  Replacement: 'Background Replacement',
};

