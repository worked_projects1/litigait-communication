
/***
 *
 * Input Field
 *
 */


import React from 'react';
import TextField from '@mui/material/TextField';
import Styles from './styles';
import { ErrorMessage } from 'formik';

export default function InputField({ field, form, ...props }) {

  const { classes } = Styles();

  return (
    <div style={{}}>
      <TextField
        fullWidth
        variant={props.variant || "standard"}
        className={classes.fieldColor}
        label={<span className={classes.textSize} >{props.label}</span>}
        {...field} {...props} />
      <ErrorMessage component="div" name={field.name} className={classes.error} />
    </div>

  )
}
