

import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
  fieldColor: {
    "&.Mui-focused": {
      borderBottomColor: '#2ca01c',
    },
    '& :after': {
      borderBottomColor: '#2ca01c !important',
    },
    '& :before': {
      borderBottomColor: '#2ca01c !important',
    },
    color: '#1d1e1c',
    '& label.Mui-focused': {
      color: '#1d1e1c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    },
  },
  input: {
    "&:-webkit-autofill": {
      WebkitBoxShadow: "0 0 0 1000px white inset"
    }
  },
  textSize: {
    fontSize: '14px',
  },
  error: {
    fontSize: '14px',
    color: 'red',
    marginTop: '5px'
  }
}));

// TODO jss-to-tss-react codemod: usages of this hook outside of this file will not be converted.
export default useStyles;