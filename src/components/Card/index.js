
import React from 'react';
import { SmallText, StyledCard } from './Styled';

const Card = ({
  header,
  title,
  description,
  smallText,
}) => (
  <StyledCard>
    {header && <div className="ch-header">{header}</div>}
    <div className="ch-body">
      <div className="ch-title">{title}</div>
      <div className="ch-description">{description}</div>
      {smallText && <SmallText>{smallText}</SmallText>}
    </div>
  </StyledCard>
);

export default Card;
