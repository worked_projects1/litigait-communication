

import React, { useEffect, useState } from 'react';
import {
    getDeviceId
} from 'amazon-chime-sdk-component-library-react';
import {
    Grid,
    IconButton,
    MenuItem,
    Menu
} from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const DeviceInput = ({
    onChange,
    label,
    devices,
    selectedDevice,
    notFoundMsg,
    classNameForHeader,
    classNameForListItem,
    iconClassName
}) => {
    const [selectedDeviceId, setSelectedDeviceId] = useState('');
    const [anchorEl, setAnchorEl] = useState(null);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    useEffect(() => {
        const getSelectedDeviceId = async () => {
            const selectedDeviceId = await getDeviceId(selectedDevice);
            setSelectedDeviceId(selectedDeviceId);
        };

        getSelectedDeviceId();
    }, [selectedDevice]);

    const deviceList = devices.map((device) => ({
        value: device.deviceId,
        label: device.label,
    }));

    const options = deviceList.length
        ? deviceList
        : [{ value: 'not-available', label: notFoundMsg }];

    const selectDevice = (e) => {
        const deviceId = e;

        if (deviceId === 'not-available') {
            return;
        }
        onChange(deviceId);
    };

    return (
        <Grid className='transparentGrid'>
            <IconButton size="small" style={{}} onClick={handleClick} className={iconClassName}>
                <KeyboardArrowDownIcon fontSize="25px" style={{ color: '#e95d0c' }} />
            </IconButton>
            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem className={classNameForHeader} disabled>
                    {label}
                </MenuItem>
                {options.map((e, i) => <MenuItem
                    key={`${e.value}_${i}`}
                    className={classNameForListItem}
                    selected={selectedDeviceId === e.value}
                    onClick={() => { selectDevice(e.value) }}
                >
                    {e.label}
                </MenuItem>)}
            </Menu>
        </Grid>
    );
};

export default DeviceInput;
