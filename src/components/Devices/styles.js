import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    totalContainer: {
        width: "100%",
        height: "100%",
        padding: "40px",
        justifyContent: "center",
        alignItems: "center"
    },
    previewItem: {
        width: "65%",
        height: "65%",
        border: "none",
        borderRadius: "5px",
        boxShadow: "0px 0px 8px 1px lightgrey",
        '@media(max-width: 35.5rem)': {
            width: "100% !important"
        }
    },
    buttonsItem: {
        width: "100%",
        justifyContent: "space-between",
        alignItems: "center",
        display: "flex",
        flexDirection: "column"
    },
    controleButtons: {
        width: "35%",
        margin: "25px 0px",
        justifyContent: "space-evenly",
        '@media(max-width: 35.5rem)': {
            width: "100% !important"
        }
    },
    previewVideo: {
        height: "100%"
    },
    popOver: {
        "& ul": {
            margin: 0
        }
    },
    popOverHeader: {
        fontFamily: "'Avenir-Bold' !important",
        margin: 0,
        lineHeight: "35px",
        fontSize: "14px",
        padding: "4px 12px"
    },
    popOverListItem: {
        padding: "8px 12px",
        fontSize: "14px"
    },
    buttonItem: {
        justifyContent: "center",
        alignItems: "center",
        display: "flex"
    },
    iconButton2: {
        fontSize: "15px !important",
        lineHeight: "57px",
        width: "18px",
        height: "18px",
        margin: "0px 5px",
        border: "none !important",
        "& svg": {
            width: "25px"
        },
        "&:hover": {
            width: "18px",
            height: "18px"
        },
        "&:focus::before": {
            width: "16px",
            height: "16px"
        },
        "&:focus::after": {
            width: "18px",
            height: "18px",
        }
    },
    downIcon: {
        padding: "0px",
        backgroundColor: "transparent !important"
    },
    transparentGrid: {
        backgroundColor: "transparent !important"
    },
    cancelBtn: {
        marginTop: "20px",
        fontFamily: 'Avenir-Regular',
        fontWeight: "bold",
        marginRight: "12px",
        paddingLeft: "25px",
        borderRadius: "20px",
        paddingRight: "25px",
        backgroundColor: "#2ca01c"
    }
})
);

export default useStyles;