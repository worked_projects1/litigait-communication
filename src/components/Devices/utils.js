import {
    DefaultDeviceController,
    isAudioTransformDevice,
    isVideoTransformDevice,
} from 'amazon-chime-sdk-js';

export const getDeviceId = async (
    device
) => {
    if (!device) {
        return '';
    }

    let intrinsicDevice;

    if (isAudioTransformDevice(device) || isVideoTransformDevice(device)) {
        intrinsicDevice = await device.intrinsicDevice();
    } else {
        intrinsicDevice = device;
    }
    const deviceId = DefaultDeviceController.getIntrinsicDeviceId(
        intrinsicDevice
    );

    return deviceId;
};