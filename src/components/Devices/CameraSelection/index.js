
import React from 'react';
import { useVideoInputs, useLogger, useMeetingManager, Caret } from 'amazon-chime-sdk-component-library-react';
import DeviceInput from '../DeviceInput';

export const CameraSelection = ({
    notFoundMsg = 'No camera devices found',
    label = 'Camera source',
    classNameForIcon,
    classNameForPopover,
    classNameForHeader,
    classNameForListItem,
    iconClassName,
    ...rest
}) => {
    const logger = useLogger();
    const { devices, selectedDevice } = useVideoInputs();
    const meetingManager = useMeetingManager();

    const handleSelect = async (deviceId) => {
        try {
            await meetingManager.startVideoInputDevice(deviceId);
        } catch (error) {
            logger.error('CameraSelection failed to select camera');
        }
    };

    return (
        <DeviceInput
            label={label}
            onChange={handleSelect}
            devices={devices}
            selectedDevice={selectedDevice}
            notFoundMsg={notFoundMsg}
            icon={<Caret />}
            classNameForIcon={classNameForIcon}
            classNameForPopover={classNameForPopover}
            classNameForHeader={classNameForHeader}
            classNameForListItem={classNameForListItem}
            iconClassName={iconClassName}
            {...rest}
        />
    );
};

export default CameraSelection;
