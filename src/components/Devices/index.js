import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';
import {
    // PrimaryButton,
    Label,
    useMeetingManager,
    Modal,
    ModalBody,
    ModalHeader,
    useSelectVideoQuality
} from 'amazon-chime-sdk-component-library-react';
// import styled from 'styled-components';
// import routes from '../../constants/routes';
import Card from '../Card';
import { useAppState } from '../../providers/AppStateProvider';
// import './Style.css';
import PreviewVideo from "./PreviewVideo";
import CameraSelection from "./CameraSelection";
import MicSelection from "./MicSelection";
import SpeakerSelection from "./SpeakerSelection";
import { Grid, IconButton, Button } from "@mui/material";
import MicIcon from '@mui/icons-material/Mic';
import MicOffIcon from '@mui/icons-material/MicOff';
import VideocamIcon from '@mui/icons-material/Videocam';
import VideocamOffIcon from '@mui/icons-material/VideocamOff';
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import VolumeOffIcon from '@mui/icons-material/VolumeOff';
import useStyles from './styles';

const Devices = () => {
    const meetingManager = useMeetingManager();
    const navigate = useNavigate();
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('');
    const { meetingId, localUserName, cameraEnable, setCameraEnable, micMute, setMicMute, muteSpeaker, setMuteSpeaker } = useAppState();
    const videoQuality = useSelectVideoQuality();
    const { classes } = useStyles();

    const handleJoinMeeting = async () => {
        setIsLoading(true);
        try {
            await meetingManager.start();
            videoQuality('720p');
            setIsLoading(false);
            navigate(`/meeting/${meetingId}`);
        } catch (error) {
            setIsLoading(false);
            setError((error).message);
        }
    };

    useEffect(() => {
        setCameraEnable(true);
        setMicMute(true);
        setMuteSpeaker(true);
    }, [setCameraEnable, setMicMute, setMuteSpeaker])


    //     const ButtonPrimary = styled(PrimaryButton)`
    //   background-color: #2ca01c;
    //   border: 0.03125rem solid #2ca01c;
    //   &:hover {
    //     background-color: #2ca01c;
    //     border: 0.03125rem solid #2ca01c;
    //   }
    //   &:focus {
    //     background-color: #2ca01c;
    //     border: 0.03125rem solid #2ca01c;
    //   }
    //   &:active {
    //     background-color: #2ca01c;
    //     border: 0.03125rem solid #2ca01c;
    //   }
    //   `;

    return (
        <Grid
            container
            name="totalContainer"
            // className="totalContainer"
            className={classes.totalContainer}
        >
            <Grid
                name="previewItem"
                // className="previewItem"
                className={classes.previewItem}
            >
                {cameraEnable ? <PreviewVideo
                    // className="previewVideo"
                    className={classes.previewVideo}
                /> : null}
            </Grid>
            <Grid
                name="buttonsItem"
                // className="buttonsItem"
                className={classes.buttonsItem}
            >
                <Grid
                    name="controleButtons"
                    container
                    layout="equal-columns"
                    // className="controleButtons"
                    className={classes.controleButtons}
                >
                    <Grid
                        // className="buttonItem"
                        className={classes.buttonItem}
                    >
                        <IconButton
                            size="50"
                            edge="start"
                            sx={{
                                //     border: "1px solid #e95d0c",
                                marginRight: "2px"
                            }}
                            onClick={() => {
                                setMicMute(!micMute);
                            }}
                        >
                            {micMute ? <MicIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            /> :
                                <MicOffIcon
                                    fontSize="medium"
                                    sx={{
                                        // color: '#e95d0c',
                                        // color: '#fff',
                                        fontSize: "23px"
                                    }}
                                />}
                        </IconButton>
                        <MicSelection
                            // classNameForIcon={"iconButton2"}
                            // classNameForPopover={"popOver"}
                            // classNameForHeader={"popOverHeader"}
                            // classNameForListItem={"popOverListItem"}
                            // iconClassName={"downIcon"}
                            classNameForIcon={classes.iconButton2}
                            classNameForPopover={classes.popOver}
                            classNameForHeader={classes.popOverHeader}
                            classNameForListItem={classes.popOverListItem}
                            iconClassName={classes.downIcon}
                        />
                    </Grid>
                    <Grid
                        // className="buttonItem"
                        className={classes.buttonItem}
                    >
                        <IconButton
                            size="50"
                            edge="start"
                            sx={{
                                //     border: "1px solid #e95d0c",
                                marginRight: "2px"
                            }}
                            onClick={() => {
                                setCameraEnable(!cameraEnable);
                            }}
                        >
                            {cameraEnable ? <VideocamIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            /> :
                                <VideocamOffIcon
                                    fontSize="medium"
                                    sx={{
                                        // color: '#e95d0c',
                                        // color: '#fff',
                                        fontSize: "23px"
                                    }}
                                />}
                        </IconButton>

                        <CameraSelection
                            // classNameForIcon={"iconButton2"}
                            // classNameForPopover={"popOver"}
                            // classNameForHeader={"popOverHeader"}
                            // classNameForListItem={"popOverListItem"}
                            // iconClassName={"downIcon"}
                            classNameForIcon={classes.iconButton2}
                            classNameForPopover={classes.popOver}
                            classNameForHeader={classes.popOverHeader}
                            classNameForListItem={classes.popOverListItem}
                            iconClassName={classes.downIcon}
                        />
                    </Grid>
                    <Grid
                        // className="buttonItem"
                        className={classes.buttonItem}
                    >
                        <IconButton
                            size="50"
                            edge="start"
                            sx={{
                                //     border: "1px solid #e95d0c",
                                marginRight: "2px"
                            }}
                            onClick={() => {
                                setMuteSpeaker(!muteSpeaker);
                            }}
                        >
                            {muteSpeaker ? <VolumeUpIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            /> :
                                <VolumeOffIcon
                                    fontSize="medium"
                                    sx={{
                                        // color: '#e95d0c',
                                        // color: '#fff',
                                        fontSize: "23px"
                                    }}
                                />}
                        </IconButton>
                        <SpeakerSelection
                            // classNameForIcon={"iconButton2"}
                            // classNameForPopover={"popOver"}
                            // classNameForHeader={"popOverHeader"}
                            // classNameForListItem={"popOverListItem"}
                            // iconClassName={"downIcon"}
                            classNameForIcon={classes.iconButton2}
                            classNameForPopover={classes.popOver}
                            classNameForHeader={classes.popOverHeader}
                            classNameForListItem={classes.popOverListItem}
                            iconClassName={classes.downIcon}
                        />
                    </Grid>
                </Grid>
                <Grid
                    container
                    alignItems="center"
                    flexDirection="column"
                >
                    <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        // className={"cancelBtn"}
                        className={classes.cancelBtn}
                        onClick={handleJoinMeeting}>
                        {isLoading ? 'Loading...' : 'Continue'}
                    </Button>
                    <Label style={{ margin: '.75rem 0 0 0' }}>
                        {/* Joining meeting <b>{meetingId}</b> as <b>{localUserName}</b> */}
                        continue with this devices
                    </Label>
                </Grid>
            </Grid>
            {error && (
                <Modal size="md" onClose={() => setError('')}>
                    <ModalHeader title={`Meeting ID: ${meetingId}`} />
                    <ModalBody>
                        <Card
                            title="Unable to join meeting"
                            description="There was an issue in joining this meeting. Check your connectivity and try again."
                            smallText={error}
                        />
                    </ModalBody>
                </Modal>
            )}
        </Grid>
    )
}

export default Devices;