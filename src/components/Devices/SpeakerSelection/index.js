
import React from 'react';
import { useAudioOutputs, useLogger, useMeetingManager, Caret } from 'amazon-chime-sdk-component-library-react';
import DeviceInput from '../DeviceInput';

export const SpeakerSelection = ({
    notFoundMsg = 'No speaker devices found',
    label = 'Speaker source',
    onChange,
    classNameForIcon,
    classNameForPopover,
    classNameForHeader,
    classNameForListItem,
    iconClassName,
    ...rest
}) => {
    const logger = useLogger();
    const { devices, selectedDevice } = useAudioOutputs();
    const meetingManager = useMeetingManager();

    const handleSelect = async (deviceId) => {
        try {
            await meetingManager.startAudioOutputDevice(deviceId);
            onChange && onChange(deviceId);
        } catch (error) {
            logger.error('SpeakerSelection failed to select speaker');
        }
    };

    return (
        <DeviceInput
            label={label}
            devices={devices}
            onChange={handleSelect}
            selectedDevice={selectedDevice}
            notFoundMsg={notFoundMsg}
            icon={<Caret />}
            classNameForIcon={classNameForIcon}
            classNameForPopover={classNameForPopover}
            classNameForHeader={classNameForHeader}
            classNameForListItem={classNameForListItem}
            iconClassName={iconClassName}
            {...rest}
        />
    );
};

export default SpeakerSelection;
