
import React from 'react';
import { useAudioInputs, useLogger, useMeetingManager, Caret } from 'amazon-chime-sdk-component-library-react';
import DeviceInput from '../DeviceInput';


export const MicSelection = ({
    notFoundMsg = 'No microphone devices found',
    label = 'Microphone source',
    classNameForIcon,
    classNameForPopover,
    classNameForHeader,
    classNameForListItem,
    iconClassName,
    ...rest
}) => {
    const logger = useLogger();
    const { devices, selectedDevice } = useAudioInputs();
    const meetingManager = useMeetingManager();

    const handleSelect = async (deviceId) => {
        try {
            await meetingManager.startAudioInputDevice(deviceId);
        } catch (error) {
            logger.error('MicSelection failed to select mic');
        }
    };

    return (
        <DeviceInput
            label={label}
            onChange={handleSelect}
            devices={devices}
            selectedDevice={selectedDevice}
            notFoundMsg={notFoundMsg}
            icon={<Caret />}
            classNameForIcon={classNameForIcon}
            classNameForPopover={classNameForPopover}
            classNameForHeader={classNameForHeader}
            classNameForListItem={classNameForListItem}
            iconClassName={iconClassName}
            {...rest}
        />
    );
};

export default MicSelection;
