import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import {
    useAudioVideo,
    useVideoInputs,
    useLocalVideo,
    useLogger,
    useMeetingManager,
    VideoTile
} from 'amazon-chime-sdk-component-library-react';

const StyledPreview = styled(VideoTile)`
  height: 100%;
  background: unset;

  video {
    position: static;
  }
`;

export const PreviewVideo = (props) => {
    const logger = useLogger();
    const audioVideo = useAudioVideo();
    const { selectedDevice } = useVideoInputs();
    const videoEl = useRef(null);
    const meetingManager = useMeetingManager();
    const { setIsVideoEnabled } = useLocalVideo();

    useEffect(() => {
        const videoElement = videoEl.current;
        return () => {
            if (videoElement) {
                audioVideo?.stopVideoPreviewForVideoInput(videoElement);
                audioVideo?.stopVideoInput();
                setIsVideoEnabled(false);
            }
        };
    }, [audioVideo]);

    useEffect(() => {
        async function startPreview() {
            if (!audioVideo || !selectedDevice || !videoEl.current) {
                return;
            }

            try {
                await meetingManager.startVideoInputDevice(selectedDevice);
                audioVideo.startVideoPreviewForVideoInput(videoEl.current);
                setIsVideoEnabled(true);
            } catch (error) {
                logger.error('Failed to start video preview');
            }
        }

        startPreview();
    }, [audioVideo, selectedDevice]);

    return <StyledPreview {...props} ref={videoEl} />;
};

export default PreviewVideo;
