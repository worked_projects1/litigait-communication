import React from "react";
import { Button } from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import useStyles from "./styles";

const FooterButton = (props) => {
    const { showButton, type, btnLabel, fun } = props;
    const { classes } = useStyles();

    return showButton ? <Button
        type={type}
        variant="contained"
        color="primary"
        // disabled={submitting || !invalid || !dirty}
        onClick={() => fun || {}}
        className={classes.cancelBtn}>

        {btnLabel}
    </Button> :
        <LoadingButton loading variant="outlined" className={classes.loadingBtn} />
}

export default FooterButton;