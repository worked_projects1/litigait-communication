/**
 * 
 * Edit Record Form
 * 
 */

import React, { useEffect } from 'react';
import * as yup from 'yup';
import { Link } from 'react-router-dom';
import { ImplementationFor } from './utils';
import { Grid, Button } from '@mui/material';
import Styles from './styles';
import LoadingButton from '@mui/lab/LoadingButton';
import Error from '../Error';
import { Field, useFormik, FormikProvider } from 'formik';

/**
 * 
 * @param {object} props 
 * @returns 
 */
const validationSchema = yup.object({
    title: yup
        .string('Enter your Title')
        .required('Required'),
    name: yup
        .string('Enter your Name')
        .required('Required'),
});


function EditRecordForm(props) {

    const { classes } = Styles();
    const { handleSubmit, pristine, submitting, fields, path, error, locationState, btnLabel, invalid, destroy, spinner, cancelBtn, updateBtn, submitBtn, enableSubmitBtn, noNeedCancel } = props;

    const formik = useFormik({
        initialValues: {
            name: '',
            title: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            handleSubmit(values);
        },
    });

    const resetForm = () => {
        // Reset the form to initial values and clear touched fields
        formik.resetForm({
            values: {
                name: '',
                title: '',
            }, touched: {}
        });
    };

    useEffect(() => {
        // This will be called when the component unmounts
        return () => {
            resetForm(); // Reset the form when the component unmounts
        };
    }, []);

    return (<div>
        <FormikProvider value={formik}>
            <form onSubmit={formik.handleSubmit}>
                <Grid container spacing={3}>
                    {(fields || []).map((field, index) => {
                        const InputComponent = ImplementationFor[field.type];
                        return <Grid key={index} item xs={12}>
                            <Field
                                name={field.value}
                                label={field.label}
                                component={InputComponent}
                                {...formik.getFieldProps(field.value)}
                            />
                        </Grid>
                    })}
                    <Grid item xs={12}>
                        {error && <Error errorMessage={error} /> || ''}
                    </Grid>
                </Grid>
                <Grid className={classes.footer}>
                    {submitBtn && typeof submitBtn === 'function' ? React.createElement(submitBtn, Object.assign({}, { ...props, classes }, { submitting: formik.isSubmitting, invalid: formik.isValid, dirty: formik.dirty })) : <Button
                        type="submit"
                        disabled={!enableSubmitBtn && (pristine || submitting) || !enableSubmitBtn && (!pristine && invalid)}
                        variant="contained"
                        color="primary"
                        className={updateBtn ? updateBtn : classes.submitBtn}>
                        {(submitting || spinner) && <LoadingButton loading variant="outlined" /> || btnLabel || 'Update'}
                    </Button>}
                    {!noNeedCancel ? cancelBtn && React.createElement(cancelBtn) ||
                        <Link to={path && locationState && { pathname: path, state: { ...locationState } } || null}>
                            <Button
                                type="button"
                                variant="contained"
                                color="primary"
                                className={classes.cancelBtn}>
                                Cancel
                            </Button>
                        </Link> : null}
                </Grid>
            </form>
        </FormikProvider>
    </div>)

}

export default EditRecordForm;