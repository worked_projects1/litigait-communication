import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    button: {
        color: '#2DA01D',
        fontWeight: 'bold',
        fontFamily: 'Avenir-Bold',
    },
    description: {
        color: '#000000',
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    },
}));


// TODO jss-to-tss-react codemod: usages of this hook outside of this file will not be converted.
export default useStyles;