
const awsPath = '/Prod';
export const rootPath = window.location.href.includes(awsPath)
  ? `${awsPath}/`
  : '/';

const routes = {
  HOME: `${rootPath}`,
  OTP_FORM: `${rootPath}video_call/:id`,
  DEVICE: `${rootPath}devices`,
  MEETING: `${rootPath}meeting/:id`,
};

export default routes;
