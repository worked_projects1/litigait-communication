
export default function getFormattedOptionsForSelect(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  jsonObject
) {
  const formattedJSONObject = Object.entries(jsonObject).map(entry => ({
    value: entry[0],
    label: entry[1],
  }));
  return formattedJSONObject;
}
