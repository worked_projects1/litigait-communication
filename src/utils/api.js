
import axios from 'axios';

const api = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'http://localhost:3000/test/' : 'https://staging-api.esquiretek.com/',
  timeout: 40000,
  headers: { Accept: 'application/json' },
});

export function setAuthToken(authToken) {
  api.defaults.headers.common['Authorization'] = authToken;
}

export default api;

export function fetchMeeting(
  meetingId,
  name,
  region,
  echoReductionCapability = false
) {
  const params = {
    title: encodeURIComponent(meetingId),
    name: encodeURIComponent(name),
    region: encodeURIComponent(region),
    ns_es: String(echoReductionCapability),
  };

  return api.post('/rest/video-calls/join', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export async function getAttendee(
  meetingId,
  chimeAttendeeId
) {
  const params = {
    // title: encodeURIComponent(meetingId),
    attendee_id: encodeURIComponent(chimeAttendeeId),
  };

  return await api.post('/rest/meeting_scheduler/getMeetingAttendee', params).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function endMeeting(meetingId) {
  const params = {
    title: encodeURIComponent(meetingId),
  };

  return api.post('/rest/meeting_scheduler/stopChimeVideoCallMeeting', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export function recordingVideo(meetingId) {
  const params = {
    title: encodeURIComponent(meetingId),
  };
  return api.post('/rest/meeting_scheduler/start-recording', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export function endrecordingVideo(meetingId) {
  const params = {
    title: encodeURIComponent(meetingId),
  };
  return api.post('/rest/meeting_scheduler/stop-recording', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export function getrecordingVideo(meetingId) {
  const params = {
    title: encodeURIComponent(meetingId),
  };

  return api.post('/rest/video-calls/get_capture', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export function concatenateVideo(meetingId) {
  const params = {
    title: encodeURIComponent(meetingId),
  };

  return api.put('/rest/video-calls/capture_concatenation', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export async function startTranscript(meetingId) {
  const params = {
    meeting_id: meetingId,
  };
  return api.post('/rest/video-calls/start_meetingTranscript', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export async function stopTranscript(meetingId) {
  const params = {
    title: meetingId,
  };
  return api.post('/rest/video-calls/stop_meetingTranscript', params).then((response) => response.data).catch((error) => Promise.reject(error));

}

export function getToken() {
  const data = api.get('getUserToken').then((response) => response.data).catch((error) => Promise.reject(error));

  return {
    response: data
  };
}

export const createGetAttendeeCallback = (meetingId) =>
  (chimeAttendeeId) =>
    getAttendee(meetingId, chimeAttendeeId);
