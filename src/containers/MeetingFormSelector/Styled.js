
import styled from 'styled-components';

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  // box-shadow: 0 0.25rem 0.5rem 0 rgba(0, 0, 0, 0.2),
      0 0.375rem 1.25rem 0 rgba(0, 0, 0, 0.2);
  border-radius: 0.25rem;
  max-width: 70%;
  min-height: 70%;
  @media (max-width: 600px) {
    max-width: 90%;
  }
  @media (min-width: 600px) and (min-height: 600px) {
    // max-width: 30rem;
    max-width: 90%;
    border-radius: 0.25rem;
    // box-shadow: 0 0.25rem 0.5rem 0 rgba(0, 0, 0, 0.2),
      0 0.375rem 1.25rem 0 rgba(0, 0, 0, 0.2);
  }
`;

export const StyledDiv = styled.div`
  padding: 2rem;
  flex: 1;

  @media (min-width: 600px) and (min-height: 600px) {
    padding: 3rem 3rem 2rem;
  }
`;
