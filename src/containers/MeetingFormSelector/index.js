
import React from 'react';
import MeetingForm from '../MeetingForm';
import { StyledDiv, StyledWrapper } from './Styled';
import MeetingEntryForm from '../MeetingEntryForm';
import routes from '../../constants/routes';
import { useLocation, matchPath } from 'react-router-dom';

const MeetingFormSelector = () => {
  const { pathname } = useLocation();

  return (
    <StyledWrapper>
      <StyledDiv>
        {/* {matchPath(routes.OTP_FORM, pathname) ? */}
        <MeetingEntryForm />
        {/* // : <MeetingForm />
      } */}
      </StyledDiv>
    </StyledWrapper>
  );
};

export default MeetingFormSelector;
