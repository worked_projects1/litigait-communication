
import React from 'react';
import Messages from './Messages';
import ChatInput from './ChatInput';
import { StyledChat, StyledTitle, StyledHeader, StyledChatContainer } from './Styled';
// import { IconButton, Remove } from 'amazon-chime-sdk-component-library-react';
import { IconButton, Grid, Divider } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
// import './styles.css';
import { createTheme } from '@mui/system';
import { useMediaQuery } from '@mui/material';

export default function Chat(props) {
  const { navigationControl } = props;
  const { toggleChat } = navigationControl;
  const theme = createTheme();
  const sm = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <StyledChatContainer>
      {/* <Grid
      className="chatTotalContainer"
      container
    > */}
      {/* <StyledChat className="chat">
        <StyledTitle> */}
      <Grid item
        style={{
          height: '8%'
        }}
      >
        <StyledHeader
        >
          <Grid className="ch-title" item xs={11}>Chat</Grid>
          <Grid className="close-button" item xs={1}>
            {/* <IconButton icon={<Remove />} label="Close" onClick={toggleChat} /> */}
            <IconButton
              label={"Close"}
              onClick={toggleChat}
              size="small"
              sx={{
                // border: "1px solid #e95d0c",
                padding: '2px',
                // backgroundColor: "#e95d0c",
                // backgroundColor: "rgb(152 151 151 / 25%)"
              }}
            >
              <CloseIcon
                // style={{ color: '#e95d0c', fontSize: "20px" }}
                sx={{
                  // color: '#e95d0c',
                  // color: '#fff',
                  fontSize: "16px"
                }}
              />
            </IconButton>
          </Grid>
        </StyledHeader>
      </Grid>
      {/* </StyledTitle> */}
      <Grid
        item
        className='messageContainer'
        // style={{
        //   height: sm ? '90%' : '80%'
        // }}
        style={{
          height: '84%'
        }}
      >
        <Messages />
      </Grid>
      <Grid
        item
        style={{
          height: '8%'
        }}
      >
        <ChatInput />
      </Grid>
      {/* </StyledChat> */}
      {/* </Grid> */}
    </StyledChatContainer>
  );
}
