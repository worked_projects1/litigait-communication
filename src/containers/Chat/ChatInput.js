
import { Input } from 'amazon-chime-sdk-component-library-react';
import React, { useState } from 'react';
import { useDataMessages } from '../../providers/DataMessagesProvider';
import { StyledChatInputContainer, CustomisedTextField } from './Styled';
import SendIcon from '@mui/icons-material/Send';
import { IconButton, TextField } from '@mui/material';

export default function ChatInput() {
  const [message, setMessage] = useState('');
  const { sendMessage } = useDataMessages();

  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  };

  // TODO: Due to mismatch in React versions installed in demo vs the one onKeyPress accepts in component library
  // there is a problem with KeyboardEvent type here.
  // For now use, any as type and cast internally to KeyboardEvent.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleKeyPress = (event) => {
    if ((event).key === 'Enter') {
      sendMessage(message);
      setMessage('');
    }
  };

  return (
    <StyledChatInputContainer>
      {/* <Input
        value={message}
        onChange={handleMessageChange}
        onKeyPress={handleKeyPress}
        placeholder="Message all attendees"
      /> */}
      <CustomisedTextField
        fullWidth
        variant={"outlined"}
        label=""
        value={message}
        onChange={handleMessageChange}
        onKeyPress={handleKeyPress}
      />
      <IconButton
        label={"Close"}
        onClick={() => {
          sendMessage(message);
          setMessage('');
        }}
        size="small"
      >
        <SendIcon
          // style={{ color: '#e95d0c', fontSize: "20px" }}
          sx={{
            // color: '#e95d0c',
            // color: '#fff',
            fontSize: "20px"
          }}
        />
      </IconButton>
    </StyledChatInputContainer>
  );
}
