
import styled from 'styled-components';
import { TextField, Grid } from '@mui/material';

export const CustomisedTextField = styled(TextField)`
& label.Mui-focused {
  border-color: #e95d0c;
}
& .MuiOutlinedInput-root {
  &.Mui-focused fieldset {
    border-color: #e95d0c;
  }
}
margin-right: 10px !important;
`;

export const StyledChatContainer = styled(Grid)`
    width: 18.5rem;
    border-radius: 5px;
    padding: 8px;
    margin-bottom: 0px;
    font-family: 'Avenir-Bold';
    height: 100%;
    flex-direction: column !important;
    display: flex;
    @media(max-width: 35.5rem) {
      // body .chatTotalContainer {
          width: 100%;
      // }
  }
`;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const StyledChat = styled.aside`
  display: grid;
  grid-template-rows: auto 1fr auto;
  grid-template-areas:
    'chat-header'
    'messages'
    'chat-input';
  width: 100%;
  height: 100%;
  padding-bottom: 1rem;
  overflow-y: auto;
  box-shadow: 1rem 1rem 3.75rem 0 rgba(0, 0, 0, 0.1);
  border-top: 0.0625rem solid ${(props) => props.theme.chat.containerBorder};
  border-left: 0.0625rem solid ${(props) => props.theme.chat.containerBorder};
  border-right: 0.0625rem solid ${(props) => props.theme.chat.containerBorder};
  ${({ theme }) => theme.mediaQueries.min.md} {
    width: ${(props) => props.theme.chat.maxWidth};
  }
`;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const StyledTitle = styled.div`
  grid-area: chat-header;
  position: relative;
  display: flex;
  align-items: center;
  padding: 0.75rem 1rem;
  font-family: 'Avenir-Bold';
  border-bottom: 0.0625rem solid ${(props) => props.theme.chat.headerBorder};
  .ch-title {
    font-size: 14px;
    color: ${(props) => props.theme.chat.primaryText};
  }
  .close-button {
    margin-left: auto;
    display: flex;
      > span {
        > button {
          border: none;
        }
    }
    > * {
      margin-left: 0.5rem;
    }
  }
`;

export const StyledHeader = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding: 0.75rem 1rem;
  margin-bottom: 0.5rem;
  border-bottom: 0.0625rem solid ${(props) => props.theme.chat.headerBorder};

  .ch-title {
    font-size: 0.875rem;
    color: ${(props) => props.theme.chat.primaryText};
    
  }

  .close-button {
    margin-left: auto;
    display: flex;

    > * {
      margin-left: 0.5rem;
    }

    
  }

`;

export const StyledChatInputContainer = styled.div`
  padding: 0px 5px 0px 5px;
  grid-area: chat-input;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 0.75rem;

  .ch-input-wrapper {
    width: 90%;

    .ch-input {
      width: 100%;
    }
  }
`;

export const StyledMessages = styled.div`
  grid-area: messages;
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;
  row-gap: 0.5rem;
`;
