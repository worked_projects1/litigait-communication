/**
 * 
 * Home Page
 * 
 */


import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
// import styled from 'styled-components';
import api from '../../utils/api';
import {
    DeviceLabels,
    useMeetingManager,
    useLogger,
    Severity,
    ActionType,
    useNotificationDispatch
} from 'amazon-chime-sdk-component-library-react';
import { DefaultBrowserBehavior, MeetingSessionConfiguration, VideoPriorityBasedPolicy, VideoAdaptiveProbePolicy } from 'amazon-chime-sdk-js';
import routes from '../../constants/routes';
import DevicePermissionPrompt from '../DevicePermissionPrompt';
import { getAttendee } from '../../utils/api';
import { useAppState } from '../../providers/AppStateProvider';
import meetingConfig from '../../meetingConfig';
import {
    Grid, Typography, Button,
    // FilledInput,
    TextField
} from '@mui/material';
import useStyles from './styles';
import CustomisedCircularProgress from '../../components/CustomisedCircularProgress';
import { useParams } from 'react-router-dom';

// const CharacterInput = styled.input.attrs({ type: 'text' })`
//    min-height: 3em;
//    font-size: 1.5em;
//    font-weight: 500;
//    background: rgba(0, 12, 63, 0.05);
//    min-width: 0;
//    border: none;
//    text-align: center;
//    border-bottom: 2px solid #000C3F;
//    margin: 0 .25em;
//    color: #000C3F;
//    padding-left: .5em;
//    padding-right: .5em;
//    transition: border-color 0.2s ease-in-out;
//    width: 100%;
//    &::placeholder {
//      color: rgba(255, 255, 255, 0.5);
//      opacity: 0.5;
//    }

//    &:focus {
//      border-color: #2ca01c;
//      outline: none;
//    }

//    :disabled,
//    &[disabled] {
//      opacity: 0.5;
//      cursor: default;
//      pointer-events: none;
//    }

//    &[type='number'] {
//      width: 100%;
//    }

//    @media (max-width: 725px) {
//      font-size: 3vw;
//    }
//  `;

function MeetingEntryForm() {
    const meetingManager = useMeetingManager();
    const { id } = useParams();
    const {
        // region,
        // meetingId,
        setJoinInfo,
        // isEchoReductionEnabled,
        setMeetingId,
        setLocalUserName,
        setRegion,
        setLocalAttendee,
        theme,
        toggleTheme
    } = useAppState();
    const [error, setError] = useState();
    const [busy, setBusy] = useState(false);
    const navigate = useNavigate();
    const logger = useLogger();
    const browserBehavior = new DefaultBrowserBehavior();
    const { classes } = useStyles();
    const dispatch = useNotificationDispatch();
    const [securityCode, setSecurityCode] = React.useState("");

    const onChange = (e) => {
        setSecurityCode(e.target.value);
    }

    useEffect(() => {
        const myFunction = (e) => {
            window.prompt("Are you feeling lucky");
            // return window.confirm("You have unsaved changes, would you like to save them?", "Save & Leave").then(function (result) {
            //     // Perform the work to save the document.
            //     // resolve(); // The tab closes at this point.
            // });
            // // Display a custom confirmation dialog
            // const confirmationMessage = 'Text message?';

            // // Use the confirm method to show your custom dialog
            // // if (!window.confirm(confirmationMessage)) {
            // console.log("sss = ", e);
            // // stopRecording();
            // // meetingManager.leave();
            // e.preventDefault();
            // e.stopPropagation();
            // e.returnValue = confirmationMessage;
            // // }
            // // return confirmationMessage

            // window.setExitDialog({
            //     message: "You have unsaved changes, would you like to save them?",
            //     buttonLabel: "Save"
            // });
        }
        // if (showPopup) {
        window.addEventListener("beforeunload", myFunction);
        // }

        return () => window.removeEventListener("beforeunload", myFunction);
    }, []);

    useEffect(() => {
        if (error) {
            const timeout = setTimeout(() => {
                setError();
            }, 2000);

            return () => clearTimeout(timeout);
        }
    }, [error]);

    useEffect(() => {
        if (theme === 'dark') {
            toggleTheme();
        }// eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const createGetAttendeeCallback = (meetingId) =>
        async (chimeAttendeeId) => {
            const data = await getAttendee(meetingId, chimeAttendeeId);
            return {
                name: data.AttendeeInfo.Name,
            };
        }

    const handleJoinMeeting = async (data) => {
        const { JoinInfo } = data;
        setBusy(true);
        const titleId = id;
        setMeetingId(titleId);
        const attendeeName = JoinInfo?.name?.trim();
        setLocalUserName(attendeeName);

        meetingManager.getAttendee = createGetAttendeeCallback(titleId);

        try {

            setJoinInfo(JoinInfo);
            setLocalAttendee(JoinInfo?.Attendee);
            const meetingSessionConfiguration = new MeetingSessionConfiguration(JoinInfo?.Meeting, JoinInfo?.Attendee);
            if (
                meetingConfig.postLogger &&
                meetingSessionConfiguration.meetingId &&
                meetingSessionConfiguration.credentials &&
                meetingSessionConfiguration.credentials.attendeeId
            ) {
                const existingMetadata = meetingConfig.postLogger.metadata;
                meetingConfig.postLogger.metadata = {
                    ...existingMetadata,
                    meetingId: meetingSessionConfiguration.meetingId,
                    attendeeId: meetingSessionConfiguration.credentials.attendeeId,
                };
            }

            setRegion(JoinInfo.Meeting.MediaRegion);
            /* please check the prioritybasedpolicy, enablesimulcast, skipDeviceSelection, isWebAudioEnabled and keeplastframepaused in basic git hub */
            //--------------------------------------//
            meetingSessionConfiguration.enableSimulcastForUnifiedPlanChromiumBasedBrowsers = true; // enablesimulcast state
            //Specify the apdative probe downlink policy
            meetingSessionConfiguration.videoDownlinkBandwidthPolicy = new VideoAdaptiveProbePolicy(logger);
            if (browserBehavior.supportDownlinkBandwidthEstimation()) {
                meetingSessionConfiguration.videoDownlinkBandwidthPolicy = new VideoPriorityBasedPolicy(logger); // prioritybasedpolicy state
            }
            meetingSessionConfiguration.keepLastFrameWhenPaused = true; // keepLastFrameWhenPaused state
            const options = {
                deviceLabels: DeviceLabels.AudioAndVideo,
                enableWebAudio: true, // isWebAudioEnabled state
                skipDeviceSelection: false, //skipDeviceSelection state
            };
            //-----------------------------------//

            await meetingManager.join(meetingSessionConfiguration, options);
            navigate(routes.DEVICE);
            setBusy(false);
        } catch (error) {
            // setBusy(false);
            const payload = {
                severity: Severity.ERROR,
                message: (error)?.response?.data?.error || error?.message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
            setBusy(false);
        }
    };

    const logIn = async value => {
        setBusy(true);
        const params = {
            "title": id,
            "pass_code": value,
            "ns_es": "true"
        }
        await api.post(`rest/meeting_scheduler/startChimeVideoCallMeeting`, params).then((res) => {
            handleJoinMeeting(res.data);
            navigate(routes.DEVICE);
        }).catch((error) => {
            console.log("error1 = ", error);
            setError('PIN is not valid, please try again.');
            const payload = {
                severity: Severity.ERROR,
                message: (error)?.response?.data?.error || error?.message || 'PIN is not valid, please try again.',
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }).finally(() => {
            setSecurityCode('');
            setBusy(false);
        });

    };

    return (
        <Grid
            container
            spacing={0}
            align="center"
            justify="center"
            direction="column"
            className={classes.grid}
        >
            <Grid item >
                <img src={require(`../../images/logo1.png`)} style={{ width: "273px" }} alt="App Logo" />
                <Grid style={{ marginTop: '20px', marginBottom: '20px' }}>
                    <Typography variant="subTitle2" className={classes.title}>
                        Enter Meeting Passcode
                    </Typography>
                </Grid>
                <TextField
                    id="filled-basic"
                    label=""
                    variant="filled"
                    className={classes.fieldColor}
                    onChange={onChange}
                    inputProps={{
                        min: 0,
                        style: { textAlign: 'center' }
                    }}
                />
                <Grid item className={classes.btnGrid}>
                    <Button
                        className={classes.btnVerify}
                        type="button"
                        variant="contained"
                        color="primary"
                        disabled={securityCode === ''}
                        classes={{ disabled: classes.disabledButton }}
                        onClick={() => logIn(securityCode)}
                    >
                        Join
                    </Button>

                </Grid>
                {busy && <Grid style={{ position: 'absolute', top: '50%', left: '48%' }}>
                    <CustomisedCircularProgress />
                </Grid>}
                <DevicePermissionPrompt />
            </Grid>
        </Grid>


    );
}


export default MeetingEntryForm;