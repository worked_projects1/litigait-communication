
import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    formContainer: {
        alignItems: 'center',
        flexDirection: 'column !important',
        justifyContent: 'center'
    },
    form: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        padding: '18px',
        marginTop: '40px'
    },
    cancelBtn: {
        minWidth: '100px',
        minHeight: '35px',
        marginTop: '20px',
        fontFamily: 'Avenir-Regular',
        fontWeight: 'bold',
        marginRight: '12px',
        paddingLeft: '25px',
        borderRadius: '20px',
        paddingRight: '25px',
        backgroundColor: '#2ca01c'
    },
    loadingBtn: {
        minWidth: '100px',
        minHeight: '35px',
        marginTop: '20px',
        fontFamily: 'Avenir-Regular',
        fontWeight: 'bold',
        marginRight: '12px',
        paddingLeft: '25px',
        borderRadius: '20px',
        paddingRight: '25px',
        backgroundColor: '#2ca01c',
        "& .MuiLoadingButton-loadingIndicator": {
            color: "#FFF"
        },
    },

    meetingProvider: {
        boxShadow: '0px 0px 8px 1px lightgrey',
    },
    inputContainer: {
        justifyContent: 'center',
    },
    title: {
        fontFamily: 'Avenir-Bold !important',
        fontSize: '20px'
    },
    inputItem: {
        maxWidth: '10%',
        marginRight: '2% !important'
    },
    // totalContainer: {
    //     justifyContent: 'center',
    //     alignItems: 'center'
    // },
    fieldColor: {
        width: '230px',
        border: 'none',
        // marginTop: '24px',
        '& :after': {
            border: 'none',
            borderBottom: '0px !important'
        },
        '& :before': {
            border: 'none',
            borderBottom: '0px !important'
        },
        '& label.Mui-focused': {
            color: '#2ca01c',
            // textAlign: 'center',
            // width: '100%'
        },
        // '&.Mui-focused fieldset': {
        //     borderColor: '#2ca01c',
        // },

    },
    btnVerify: {
        fontSize: '20px',
        width: '230px',
        height: '50px',
        margin: '20px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        marginTop: '20px',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    btnVerifyDisabled: {
        fontSize: '20px',
        width: '230px',
        height: '50px',
        margin: '20px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        marginTop: '20px',
        opacity: '0.7',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    disabledButton: {
        backgroundColor: '#2ca01c !important',
        textTransform: 'none',
        marginTop: '20px',
        color: '#fff !important',
        '&:hover': {
            background: '#2ca01c',
        },
        opacity: 0.7
    },
    // btnGrid: {
    //     padding: "20px"
    // }
}));

// TODO jss-to-tss-react codemod: usages of this hook outside of this file will not be converted.
export default useStyles;