import React, { useEffect, useState } from 'react';
import {
  ActionType,
  MeetingStatus,
  Severity,
  useMeetingStatus,
  useNotificationDispatch,
  useLocalVideo,
  useToggleLocalMute,
  useLocalAudioOutput
} from 'amazon-chime-sdk-component-library-react';
import routes from '../../constants/routes';
import { useNavigate } from 'react-router-dom';
import { useAppState } from '../../providers/AppStateProvider'

const MeetingStatusNotifier = () => {
  const meetingStatus = useMeetingStatus();
  const dispatch = useNotificationDispatch();
  const [status, setStatus] = useState();
  const navigate = useNavigate();
  const { toggleMute } = useToggleLocalMute();
  const { toggleAudio } = useLocalAudioOutput();
  const { cameraEnable, setCameraEnable, micMute, setMicMute, muteSpeaker, setMuteSpeaker } = useAppState();

  const { toggleVideo } = useLocalVideo();
  const getMeetingStatusPayload = (message, severity) => {
    return {
      severity,
      message,
      autoClose: true,
      replaceAll: true,
    };
  };


  useEffect(() => {
    async function toggle() {
      if (meetingStatus === MeetingStatus.Succeeded && cameraEnable) {
        await toggleVideo();

      }
      if (meetingStatus === MeetingStatus.Succeeded && !micMute) {
        await toggleMute();

      }
      if (meetingStatus === MeetingStatus.Succeeded && !muteSpeaker) {
        await toggleAudio();

      }
    }
    toggle();
    return () => {
    }
  }, [meetingStatus]);


  useEffect(() => {
    switch (meetingStatus) {
      case MeetingStatus.Loading:
        setStatus('connecting');
        dispatch({
          type: ActionType.ADD,
          payload: getMeetingStatusPayload('Meeting connecting...', Severity.INFO),
        });
        break;
      case MeetingStatus.Succeeded:
        setStatus('connected');
        if (status === 'reconnecting') {
          dispatch({
            type: ActionType.ADD,
            payload: getMeetingStatusPayload('Meeting reconnected', Severity.SUCCESS),
          });
        } else {
          dispatch({
            type: ActionType.ADD,
            payload: getMeetingStatusPayload('Meeting connected', Severity.SUCCESS),
          });
        }
        break;
      case MeetingStatus.Reconnecting:
        setStatus('reconnecting');
        dispatch({
          type: ActionType.ADD,
          payload: getMeetingStatusPayload('Meeting reconnecting...', Severity.WARNING),
        });
        break;
      case MeetingStatus.Failed:
        setStatus('failed');
        dispatch({
          type: ActionType.ADD,
          payload: getMeetingStatusPayload(
            'Meeting failed even after reconnection attempts, redirecting to home',
            Severity.ERROR
          ),
        });
        navigate(routes.HOME);
        break;
      case MeetingStatus.TerminalFailure:
        setStatus('failed');
        dispatch({
          type: ActionType.ADD,
          payload: getMeetingStatusPayload(
            'Meeting will not reconnect due to fatal failure, redirecting to home',
            Severity.ERROR
          ),
        });
        navigate(routes.HOME);
        break;
      default:
        break;
    }
    return () => {
      setStatus(undefined);
    };
  }, [meetingStatus]);

  useEffect(() => {
    let id;
    if (status === 'reconnecting') {
      id = setInterval(() => {
        dispatch({
          type: ActionType.ADD,
          payload: getMeetingStatusPayload('Meeting reconnecting...', Severity.WARNING),
        });
      }, 10 * 1000);
    }
    return () => {
      clearInterval(id);
    };
  }, [status]);

  return null;
};

export default MeetingStatusNotifier;
