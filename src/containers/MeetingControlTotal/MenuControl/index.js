
import React, { useEffect, useState } from "react";
import {
    ControlBarButton,
    useRosterState,
    Severity,
    ActionType,
    useNotificationDispatch
} from 'amazon-chime-sdk-component-library-react';
import { useAppState } from "../../../providers/AppStateProvider";
import { Layout } from "../../../types";
import { startTranscript, stopTranscript, recordingVideo, endrecordingVideo, concatenateVideo } from '../../../utils/api';
import ContentShareControl from '../ContentShareControl';
import { Menu, MenuItem, Grid, ListItemButton, Tooltip, IconButton, } from "@mui/material";
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import CircleIcon from '@mui/icons-material/Circle';
import GridViewIcon from '@mui/icons-material/GridView';
import ViewSidebarIcon from '@mui/icons-material/ViewSidebar';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ClosedCaptionIcon from '@mui/icons-material/ClosedCaption';
import { useVideoTileGridControl } from "../../../providers/VideoTileGridProvider";
import PanoramaIcon from '@mui/icons-material/Panorama';

const MenuControl = (props) => {
    const { setMediaCapturePipeline, capturePipeline, startTranscriptFun, setStartTranscriptFun, toggleBackgroundChange, showBackgroundChange } = props;
    // const logger = useLogger();
    const { meetingId, joinInfo, toggleTheme, theme } = useAppState();
    const { layout, setLayout } = useVideoTileGridControl();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const { roster } = useRosterState();
    const [showRecordIndicator, setShowRecordIndicator] = useState(false);
    const [startRecord, setStartRecord] = useState(false);
    const dispatch = useNotificationDispatch();

    useEffect(() => {
        let attendees = Object.values(roster);
        const mediaPipeLine = attendees.find((attendee) =>
            attendee?.name?.toLowerCase().includes("recording")
        );
        if (mediaPipeLine && mediaPipeLine != -1) {
            setShowRecordIndicator(true);
        } else {
            setShowRecordIndicator(false);
        }
    }, [roster]);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const startRecording = async () => {
        try {
            if (meetingId) {
                setStartRecord(true);
                const res = await recordingVideo(meetingId);
                setMediaCapturePipeline(res.MediaCapturePipeline);
                setStartRecord(false);
            }
        } catch (error) {
            // logger.error(`Could not end meeting: ${e}`);
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }
    };

    const endRecording = async () => {
        try {
            if (meetingId) {
                setStartRecord(true);
                await endrecordingVideo(meetingId);
                setStartRecord(false);
            }
        } catch (error) {
            // logger.error(`Could not end meeting: ${e}`);
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }
    };

    const concatenate = async () => {
        try {
            if (capturePipeline.MediaPipelineArn) {
                await concatenateVideo(capturePipeline.MediaPipelineArn);
            }
        } catch (error) {
            // logger.error(`Could not end meeting: ${e}`);
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }
    };

    const handleRecord = () => {
        if (!showRecordIndicator) {
            startRecording();
        } else {
            endRecording();
        }
        // setStartRecord(!showRecordIndicator);
    }

    const startTranscription = async () => {
        try {
            if (joinInfo) {
                await startTranscript(joinInfo?.Meeting?.MeetingId);
            }
        } catch (error) {
            // logger.error(`Could not end meeting: ${e}`);
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }
    }

    const stopTranscription = async () => {
        try {
            if (meetingId) {
                await stopTranscript(meetingId);
            }
        } catch (error) {
            // logger.error(`Could not end meeting: ${e}`);
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        }
    }

    const handleTranscript = () => {
        if (!startTranscriptFun) {
            startTranscription();
        } else {
            stopTranscription();
        }
        setStartTranscriptFun(!startTranscriptFun);
    }


    return (<Grid className='transparentGrid'>
        <IconButton
            onClick={handleClick}
            size="small"
            sx={{
                // border: "1px solid #e95d0c",
                padding: '5px',
                // backgroundColor: "#e95d0c",
                // backgroundColor: "rgb(152 151 151 / 25%)"
            }}
            aria-controls={open ? 'account-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
        >
            <MoreHorizIcon
                fontSize="medium"
                sx={{
                    // color: '#e95d0c',
                    // color: '#fff',
                    fontSize: "23px"
                }}
            />
        </IconButton>
        <Menu
            anchorEl={anchorEl}
            id="account-menu"
            open={open}
            onClose={handleClose}
            onClick={handleClose}
            PaperProps={{
                elevation: 0,
                sx: {
                    overflow: 'visible',
                    // bottom: 5,
                    // left: 50
                    // filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',

                },
            }}
            //             transformOrigin={{ horizontal: 'right', vertical: 'top' }}
            //             anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}

            transformOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            anchorOrigin={{ horizontal: 'right', vertical: -12 }}
        >
            <MenuItem
                onClick={!startRecord ?
                    () => {
                        handleClose();
                        handleRecord();
                    }
                    : (e) => {
                        e.stopPropagation();
                    }
                }
                className="menupopOverItem"
                sx={{
                    opacity: startRecord ? 0.5 : 1,
                }}
            >
                <Tooltip title="Record" placement="right-start">
                    <IconButton size="small"
                        sx={{
                            // border: "1px solid #e95d0c",
                            padding: '5px',
                            // backgroundColor: "#e95d0c",
                            // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}
                    >
                        {!showRecordIndicator ? <RadioButtonCheckedIcon
                            fontSize="medium"
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "23px"
                            }}
                        />
                            : <CircleIcon
                                fontSize="medium"
                                sx={{
                                    color: 'red',
                                    fontSize: "23px"
                                }}
                            />}
                    </IconButton>
                </Tooltip>
            </MenuItem>

            {/* <MenuItem onClick={() => {
                handleClose();
                handleTranscript();

            }} className="menupopOverItem">
                <Tooltip title="Transcript" placement="right-start">
                    <IconButton size="small"
                        sx={{ 
                            border: "1px solid #e95d0c", 
                            borderRadius: '50%', 
                            padding: '5px' }}
                            >
                        <ClosedCaptionIcon fontSize="medium" sx={{ color: '#e95d0c' }} />
                    </IconButton>
                </Tooltip>
            </MenuItem> */}

            <MenuItem className="menupopOverItem" >
                <ContentShareControl anchorElParent={anchorEl} />
            </MenuItem>

            <MenuItem onClick={() => {
                handleClose();
                toggleBackgroundChange(!showBackgroundChange);

            }} className="menupopOverItem">
                <Tooltip title="Background Settings" placement="right-start">
                    <IconButton size="small"
                        sx={{
                            // border: "1px solid #e95d0c",
                            padding: '5px',
                            // backgroundColor: "#e95d0c",
                            // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}>
                        <PanoramaIcon
                            fontSize="medium"
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "23px"
                            }}
                        />
                    </IconButton>
                </Tooltip>
            </MenuItem>

            <MenuItem onClick={() => {
                handleClose();
                if (layout === Layout.Gallery) {
                    setLayout(Layout.Featured);
                } else {
                    setLayout(Layout.Gallery);
                }

            }} className="menupopOverItem">
                <Tooltip title={layout === Layout.Gallery ? "Side View" : "Grid View"} placement="right-start">
                    <IconButton size="small"
                        sx={{
                            // border: "1px solid #e95d0c",
                            padding: '5px',
                            // backgroundColor: "#e95d0c",
                            // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}
                    >
                        {layout === Layout.Gallery ? (
                            <ViewSidebarIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />
                        ) : (
                            <GridViewIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />
                        )}
                    </IconButton>
                </Tooltip>
            </MenuItem>

            <MenuItem onClick={() => {
                handleClose();
                toggleTheme();
            }} className="menupopOverItem">
                <Tooltip title={theme == "light" ? "Dark Theme" : "Light Theme"} placement="right-start">
                    <IconButton size="small"
                        sx={{
                            // border: "1px solid #e95d0c",
                            padding: '5px',
                            // backgroundColor: "#e95d0c",
                            // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}>
                        <VisibilityIcon
                            fontSize="medium"
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "23px"
                            }}
                        />
                    </IconButton>
                </Tooltip>
            </MenuItem>

        </Menu>
    </Grid>
    )
}

export default MenuControl;

