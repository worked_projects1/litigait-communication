
import React, { useEffect, useState } from 'react';
import {
    VoiceFocusTransformDevice,
} from 'amazon-chime-sdk-js';
import isEqual from 'lodash.isequal';
import {
    useToggleLocalMute,
    useAudioVideo,
    useAudioInputs,
    useLogger,
    useMeetingManager,
    useVoiceFocus,
    isOptionActive
} from 'amazon-chime-sdk-component-library-react';
import useMemoCompare from '../../../utils/use-memo-compare';
import { CircularProgress, Grid, IconButton, Menu, MenuItem, Divider } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const AudioInputVFControl = ({
    label,
    notFoundMsg = 'No microphone devices found',
    voiceFocusOnLabel = 'Voice Focus enabled',
    voiceFocusOffLabel = 'Enable Voice Focus',
    classNameForIcon,
    classNameForPopover,
    classNameForHeader,
    classNameForListItem,
    iconClassName,
    ...rest
}) => {
    const logger = useLogger();
    const audioVideo = useAudioVideo();
    const meetingManager = useMeetingManager();
    const [isLoading, setIsLoading] = useState(false);
    // When the user click on Amazon Voice Focus option, the state will change.
    const [isVoiceFocusChecked, setIsVoiceFocusChecked] = useState(false);
    // Only when the current input audio device is an Amazon Voice Focus device,
    // the state will be true. Otherwise, it will be false.
    const [isVoiceFocusEnabled, setIsVoiceFocusEnabled] = useState(false);
    const [dropdownWithVFOptions, setDropdownWithVFOptions] = useState(null);
    // const [selectedDeviceId, setSelectedDeviceId] = useState('');
    const [anchorEl, setAnchorEl] = useState(null);
    // const { muted, toggleMute } = useToggleLocalMute();
    const { isVoiceFocusSupported, addVoiceFocus } = useVoiceFocus();
    const { devices, selectedDevice } = useAudioInputs();

    const audioInputDevices = useMemoCompare(
        devices,
        (prev, next) => {
            return isEqual(prev, next);
        }
    );

    useEffect(() => {
        logger.info(
            `Amazon Voice Focus is ${isVoiceFocusEnabled ? 'enabled' : 'disabled'}.`
        );
    }, [isVoiceFocusEnabled]);

    useEffect(() => {
        // Only when the current input audio device is an Amazon Voice Focus transform device,
        // Amazon Voice Focus button will be checked.
        if (selectedDevice instanceof VoiceFocusTransformDevice) {
            setIsVoiceFocusEnabled(true);
        } else {
            setIsVoiceFocusEnabled(false);
        }
        return () => {
            if (selectedDevice instanceof VoiceFocusTransformDevice) {
                selectedDevice.stop();
            }
        };
    }, [selectedDevice]);

    useEffect(() => {
        if (!audioVideo) {
            return;
        }
        if (
            selectedDevice instanceof VoiceFocusTransformDevice &&
            isVoiceFocusEnabled
        ) {
            selectedDevice.observeMeetingAudio(audioVideo);
        }
    }, [audioVideo, isVoiceFocusEnabled, selectedDevice]);

    const handleClick = async (deviceId) => {
        try {
            if (isVoiceFocusChecked && !isLoading) {
                setIsLoading(true);
                const receivedDevice = deviceId;
                const currentDevice = await addVoiceFocus(receivedDevice);
                await meetingManager.startAudioInputDevice(currentDevice);
            } else {
                await meetingManager.startAudioInputDevice(deviceId);
            }
        } catch (error) {
            logger.error('AudioInputVFControl failed to select audio input device');
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {


        const getDropdownWithVFOptions = async () => {
            const dropdownOptions = audioInputDevices.length ? await Promise.all(
                audioInputDevices.map(async (device) => (
                    <MenuItem
                        key={device.deviceId}
                        className={classNameForListItem}
                        selected={await isOptionActive(selectedDevice, device.deviceId)}
                        onClick={async () => await handleClick(device.deviceId)}
                    >
                        <span>{device.label}</span>
                    </MenuItem>
                ))
            ) : [{ value: 'not-available', label: notFoundMsg }].map(e =>
                <MenuItem
                    key={e}
                    className={classNameForListItem}

                >
                    {e.label}
                </MenuItem>);

            if (isVoiceFocusSupported) {
                const vfOption = (
                    <MenuItem
                        key="voicefocus"
                        className={classNameForListItem}
                        selected={isVoiceFocusEnabled}
                        disabled={isLoading}
                        onClick={(e) => {
                            e.stopPropagation();
                            setIsLoading(true);
                            setIsVoiceFocusChecked((current) => !current);
                        }}
                    >
                        <>
                            {isLoading && <CircularProgress width="1.5rem" height="1.5rem" />}
                            {isVoiceFocusEnabled ? voiceFocusOnLabel : voiceFocusOffLabel}
                        </>
                    </MenuItem>
                );
                // dropdownOptions?.push(<Divider key="separator" />);
                // dropdownOptions?.push(vfOption);
            }

            setDropdownWithVFOptions(dropdownOptions);
        };

        getDropdownWithVFOptions();
    }, [
        // The contents of this dropdown depends, of course, on the current selected device,
        // but also on the Voice Focus state, including `addVoiceFocus` which is used inside
        // the click handler.
        addVoiceFocus,
        meetingManager,
        meetingManager.startAudioInputDevice,
        audioInputDevices,
        isLoading,
        isVoiceFocusEnabled,
        isVoiceFocusChecked,
        isVoiceFocusSupported,
        selectedDevice,
    ]);

    useEffect(() => {
        const onVFCheckboxChange = async () => {
            if (!selectedDevice) {
                return;
            }
            console.log("isVoiceFocusChecked 2 = ", isVoiceFocusChecked);
            try {
                let current = selectedDevice;
                if (isVoiceFocusChecked) {
                    logger.info('User turned on Amazon Voice Focus.');
                    if (typeof selectedDevice === 'string') {
                        current = await addVoiceFocus(selectedDevice);
                    }
                } else {
                    logger.info(
                        'Amazon Voice Focus is off by default or user turned off Amazon Voice Focus.'
                    );
                    if (selectedDevice instanceof VoiceFocusTransformDevice) {
                        current = selectedDevice.getInnerDevice();
                    }
                }
                await meetingManager.startAudioInputDevice(current);
            } catch (error) {
                logger.error(
                    'AudioInputVFControl failed to select audio input device onVFCheckboxChange change'
                );
            }
            setIsLoading(false);
        };

        onVFCheckboxChange();
    }, [isVoiceFocusChecked]);

    const handleOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <Grid className='transparentGrid'>
            <IconButton size="small" style={{}} onClick={handleOpen} className={iconClassName}>
                <KeyboardArrowDownIcon fontSize="25px" style={{ color: '#e95d0c' }} />
            </IconButton>
            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem className={classNameForHeader} disabled>
                    {label}
                </MenuItem>
                {dropdownWithVFOptions}
            </Menu>
        </Grid>
    );
};

export default AudioInputVFControl;
