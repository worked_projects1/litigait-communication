
import React, { useCallback } from 'react';
import { useContentShareControls, useContentShareState, ControlBarButton, ScreenShare, useAudioVideo } from 'amazon-chime-sdk-component-library-react';
import { Grid, Menu, IconButton, MenuItem, Tooltip } from '@mui/material';
import ScreenShareIcon from '@mui/icons-material/ScreenShare';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const ContentShareControl = ({
    pauseLabel = 'Pause',
    unpauseLabel = 'Unpause',
}) => {
    const { isLocalUserSharing } = useContentShareState();
    const { audioVideo } = useAudioVideo();
    const {
        paused,
        toggleContentShare,
        togglePauseContentShare,
        isLocalShareLoading
    } =
        useContentShareControls();
    const [anchorEl1, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl1);

    const dropdownOptions = [
        {
            children: <span>{paused ? unpauseLabel : pauseLabel}</span>,
            onClick: togglePauseContentShare,
        },
    ];

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    // const toggleContentShareCustom = async () => {
    //     const mediaStream = await navigator.mediaDevices.getDisplayMedia({
    //         video: true,
    //         audio: true,
    //     });
    //     toggleContentShare(mediaStream);
    // };

    // const toggleContentShare = useCallback(
    //     async (source) => {
    //         console.log("source 4353 = ", source);
    //         if (!audioVideo) {
    //             return;
    //         }

    //         if (isLocalUserSharing || isLocalShareLoading) {
    //             audioVideo.stopContentShare();
    //         } else {
    //             if (source && typeof source === 'string') {
    //                 audioVideo.startContentShareFromScreenCapture(source);
    //             } else if (source instanceof MediaStream) {
    //                 audioVideo.startContentShare(source);
    //             } else {
    //                 audioVideo.startContentShareFromScreenCapture();
    //             }
    //             dispatch({ type: ContentActionType.STARTING });
    //         }
    //     },
    //     [audioVideo, isLocalUserSharing, isLocalShareLoading]
    // );

    return (
        <>
            <Tooltip title="Screen Share" placement="right-start">
                <IconButton
                    onClick={toggleContentShare}
                    size="small"
                    sx={{
                        // border: "1px solid #e95d0c",
                        padding: '5px',
                        marginRight: '2px'
                        // backgroundColor: "#e95d0c",
                        // backgroundColor: "rgb(152 151 151 / 25%)"
                    }}

                >
                    <ScreenShareIcon
                        fontSize="medium"
                        sx={{
                            // color: '#e95d0c',
                            fontSize: "23px"
                        }}
                    />
                </IconButton>
            </Tooltip>
            <Grid className='transparentGrid'>
                {isLocalUserSharing ? <IconButton
                    size="small"
                    sx={{
                        // border: "1px solid #e95d0c",
                        // padding: '2px',
                        // backgroundColor: "#e95d0c",
                        // backgroundColor: "rgb(152 151 151 / 25%)"
                        padding: "0px",
                        backgroundColor: "transparent"
                    }}
                    onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        handleClick(e);
                    }} className={"controlDownIcon"}
                    aria-controls={open ? 'account-menu2' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? 'true' : undefined}
                >
                    <KeyboardArrowDownIcon
                        fontSize="25px" style={{ color: '#e95d0c' }}
                    />
                </IconButton> : null}
                <Menu
                    anchorEl={anchorEl1}
                    id="account-menu2"
                    open={open}
                    onClose={handleClose}
                    onClick={handleClose}
                    PaperProps={{
                        elevation: 0,
                        sx: {
                            overflow: 'visible',
                            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',

                            '& .MuiAvatar-root': {
                                width: 32,
                                height: 32,
                            },
                            '&:before': {
                                content: '""',
                                display: 'block',
                                position: 'absolute',
                                top: 0,
                                left: 14,
                                width: 10,
                                height: 10,
                                bgcolor: 'background.paper',
                                transform: 'translateY(-50%) rotate(45deg)',
                                zIndex: 0,
                            },
                        },
                    }}
                    transformOrigin={{
                        vertical: -5,
                        horizontal: 10,
                    }}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                >
                    {dropdownOptions.map((e, i) => <MenuItem onClick={() => {
                        handleClose();
                        e.onClick();
                    }} className="menupopOverItem">
                        {e.children}
                    </MenuItem>)}
                </Menu>
            </Grid>
        </>
    );
};

export default ContentShareControl;
