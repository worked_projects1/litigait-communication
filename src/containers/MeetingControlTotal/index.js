
import React from 'react';
import {
    useToggleLocalMute,
    useLocalAudioOutput,
    useLocalVideo,
} from 'amazon-chime-sdk-component-library-react';
import styled from 'styled-components';
// import MicSelection from '../../components/Devices/MicSelection';
import CameraSelection from '../../components/Devices/CameraSelection';
import SpeakerSelection from '../../components/Devices/SpeakerSelection';
// import "./styles.css";
import ExitMeetingControl from './ExitMeetingControl';
import MenuControl from './MenuControl';
import { Grid, IconButton, Tooltip } from '@mui/material';
import MicIcon from '@mui/icons-material/Mic';
import MicOffIcon from '@mui/icons-material/MicOff';
import VideocamIcon from '@mui/icons-material/Videocam';
import VideocamOffIcon from '@mui/icons-material/VideocamOff';
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import VolumeOffIcon from '@mui/icons-material/VolumeOff';
import GroupIcon from '@mui/icons-material/Group';
import ChatIcon from '@mui/icons-material/Chat';
import AudioInputVFControl from './AudioVFControl';
import VideoInputTransformControl from './VideoInputTransformControl';
import { useAppState } from '../../providers/AppStateProvider';
import useStyles from './styles';

const MeetingControlsTotal = (props) => {
    const { navigationControl } = props;
    const { toggleMute, muted } = useToggleLocalMute();
    const { isAudioOn, toggleAudio } = useLocalAudioOutput();
    const { toggleVideo, isVideoEnabled } = useLocalVideo();
    const { toggleRoster, toggleChat, setMediaCapturePipeline, capturePipeline, startTranscriptFun, setStartTranscriptFun, toggleBackgroundChange, showBackgroundChange } = navigationControl;
    const { theme } = useAppState();
    const { classes } = useStyles();

    const StyledControls = styled(Grid)`
    opacity: ${props => (props.active ? '1' : '0')};
    transition: opacity 250ms ease;
    width: 100%;
    display: flex;
   background-color: transparent !important;
    justify-content: center;
        position: absolute;
        bottom: 3%;
        left: 50%; 
        transform: translateX(-50%);
    
      
    @media screen and (max-width: 768px) {
        opacity: 1;
        position: fixed;
        height: 50px;
    }
    `;

    return (<StyledControls className="controls" active={true}>
        <Grid
            // className="controlBar"
            className={classes.controlBar}
            container
            sx={{
                backgroundColor: theme === "light" ? "#fff" : "inherit"
            }}
        >
            <Grid
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >

                <MenuControl {...Object.assign({}, {
                    setMediaCapturePipeline: setMediaCapturePipeline,
                    capturePipeline: capturePipeline,
                    startTranscriptFun: startTranscriptFun,
                    setStartTranscriptFun: setStartTranscriptFun,
                    toggleBackgroundChange: toggleBackgroundChange,
                    showBackgroundChange: showBackgroundChange
                })} />
            </Grid>
            <Grid
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >
                <Grid className='transparentGrid'>
                    <Tooltip title="Attendees" placement="top-end">
                        <IconButton
                            sx={{
                                //     // border: "1px solid #e95d0c",
                                padding: '5px',
                                //     backgroundColor: "#e95d0c",
                                //     // backgroundColor: "rgb(152 151 151 / 25%)"
                            }}
                            onClick={() => {
                                toggleRoster();
                            }}
                        >
                            <GroupIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
            <Grid
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >
                <Grid className='transparentGrid'>
                    <Tooltip title="Chat" placement="top-end">
                        <IconButton
                            sx={{
                                //     // border: "1px solid #e95d0c",
                                padding: '5px',
                                //     backgroundColor: "#e95d0c",
                                //     // backgroundColor: "rgb(152 151 151 / 25%)"
                            }}
                            onClick={() => {
                                toggleChat();
                            }}
                        >
                            <ChatIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
            <Grid
                sx={{
                    display: 'flex',
                    alignItems: 'center'
                }}
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >
                <Tooltip
                    title={!muted ? "Mute" : "Unmute"}
                    placement="top-end"
                >
                    <IconButton
                        sx={{
                            //     // border: "1px solid #e95d0c",
                            padding: '5px',
                            //     backgroundColor: "#e95d0c",
                            //     // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}
                        onClick={() => {
                            toggleMute();
                        }}
                    >
                        {!muted ? <MicIcon
                            fontSize="medium"
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "23px"
                            }}
                        /> :
                            <MicOffIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />}
                    </IconButton>
                </Tooltip>
                <AudioInputVFControl
                    label="Microphone source"
                    // classNameForIcon={"iconButton2"}
                    // classNameForPopover={"popOver"}
                    // classNameForHeader={"popOverHeader"}
                    // classNameForListItem={"popOverListItem"}
                    // iconClassName={"controlDownIcon"}
                    classNameForIcon={classes.iconButton2}
                    classNameForPopover={classes.popOver}
                    classNameForHeader={classes.popOverHeader}
                    classNameForListItem={classes.popOverListItem}
                    iconClassName={classes.controlDownIcon}
                />
            </Grid>
            <Grid
                sx={{
                    display: 'flex',
                    alignItems: 'center'
                }}
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >
                <Tooltip
                    title={!isVideoEnabled ? "Camera On" : "Camera Off"}
                    placement="top-end"
                >
                    <IconButton
                        sx={{
                            //     // border: "1px solid #e95d0c",
                            padding: '5px',
                            //     backgroundColor: "#e95d0c",
                            //     // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}
                        onClick={() => toggleVideo()}
                    >
                        {isVideoEnabled ? <VideocamIcon
                            fontSize="medium"
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "23px"
                            }}
                        /> :
                            <VideocamOffIcon
                                fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />}
                    </IconButton>
                </Tooltip>
                <VideoInputTransformControl
                    label="Camera source"
                    // classNameForIcon={"iconButton2"}
                    // classNameForPopover={"popOver"}
                    // classNameForHeader={"popOverHeader"}
                    // classNameForListItem={"popOverListItem"}
                    // iconClassName={"controlDownIcon"}
                    classNameForIcon={classes.iconButton2}
                    classNameForPopover={classes.popOver}
                    classNameForHeader={classes.popOverHeader}
                    classNameForListItem={classes.popOverListItem}
                    iconClassName={classes.controlDownIcon}
                />
            </Grid>
            <Grid
                style={{
                    display: 'flex',
                    alignItems: 'center'
                }}
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >
                <Tooltip
                    title={!isAudioOn ? "Speaker On" : "Speaker Off"}
                    placement="top-end"
                >
                    <IconButton
                        sx={{
                            //     // border: "1px solid #e95d0c",
                            padding: '5px',
                            //     backgroundColor: "#e95d0c",
                            //     // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}
                        onClick={() => toggleAudio()}
                    >
                        {isAudioOn ? <VolumeUpIcon fontSize="medium"
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "23px"
                            }}
                        /> :
                            <VolumeOffIcon fontSize="medium"
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "23px"
                                }}
                            />}
                    </IconButton>
                </Tooltip>
                <SpeakerSelection
                    // classNameForIcon={"iconButton2"}
                    // classNameForPopover={"popOver"}
                    // classNameForHeader={"popOverHeader"}
                    // classNameForListItem={"popOverListItem"}
                    // iconClassName={"controlDownIcon"}
                    classNameForIcon={classes.iconButton2}
                    classNameForPopover={classes.popOver}
                    classNameForHeader={classes.popOverHeader}
                    classNameForListItem={classes.popOverListItem}
                    iconClassName={classes.controlDownIcon}
                />
            </Grid>
            <Grid
                // className="iconGrid"
                className={classes.iconGrid}
                item
            >
                <ExitMeetingControl />
            </Grid>
        </Grid>
    </StyledControls>
    );
};

export default MeetingControlsTotal;
