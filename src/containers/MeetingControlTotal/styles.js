import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    controlBar: {
        width: "50%",
        height: "100%",
        display: "flex",
        justifyContent: "space-evenly",
        padding: "10px",
        borderRadius: "50px",
        boxShadow: "0px 0px 8px 1px lightgrey",
        margin: "0px 5px",
        '@media(max-width: 720px)': {
            width: "100%"
        }
    },
    iconButton2: {
        width: "16px",
        height: "16px",
        margin: "0px",
        border: "none !important",
        '& svg': {
            width: "25px"
        },
        "&:hover": {
            width: "16px",
            height: "16px"
        },
        "&:focus::before": {
            width: "14px",
            height: "14px"
        },
        "&:focus::after:": {
            width: "16px",
            height: "16px"
        }
    },
    controlDownIcon: {
        padding: "0px",
        backgroundColor: "transparent !important"
    },
    iconGrid: {
        display: "flex",
        backgroundColor: "transparent !important"
    },
    menupopOverItem: {
        border: "none !important",
        fontSize: "14px",
        "& div": {
            margin: "0px",
            display: "flex"
        }
    },
    popOver: {
        "& ul": {
            margin: 0
        }
    },
    popOverHeader: {
        fontFamily: "'Avenir-Bold' !important",
        margin: 0,
        lineHeight: "35px",
        fontSize: "14px",
        padding: "4px 12px"
    },
    popOverListItem: {
        padding: "8px 12px",
        fontSize: "14px"
    },

})
);

export default useStyles;