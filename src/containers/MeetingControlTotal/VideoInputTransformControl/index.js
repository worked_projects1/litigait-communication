
import { isVideoTransformDevice } from 'amazon-chime-sdk-js';
import React, { useEffect, useState } from 'react';
import isEqual from 'lodash.isequal';
import {
    useVideoInputs,
    useLocalVideo,
    useMeetingManager,
    isOptionActive,
    useLogger,
} from 'amazon-chime-sdk-component-library-react';
import useMemoCompare from '../../../utils/use-memo-compare';
import { CircularProgress, Grid, IconButton, Menu, MenuItem, Divider } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const VideoInputTransformControl = ({
    label = 'Camera source',
    notFoundMsg = 'No camera devices found',
    backgroundBlurLabel = 'Enable Background Blur',
    backgroundReplacementLabel = 'Enable Background Replacement',
    classNameForIcon,
    classNameForPopover,
    classNameForHeader,
    classNameForListItem,
    iconClassName,
    ...rest
}) => {
    const meetingManager = useMeetingManager();
    const logger = useLogger();
    const { devices, selectedDevice } = useVideoInputs();
    const { isVideoEnabled, toggleVideo } = useLocalVideo();
    // const { isBackgroundBlurSupported, createBackgroundBlurDevice } = useBackgroundBlur();
    // const { isBackgroundReplacementSupported, createBackgroundReplacementDevice } = useBackgroundReplacement();
    const [isLoading, setIsLoading] = useState(false);
    const [dropdownWithVideoTransformOptions, setDropdownWithVideoTransformOptions] = useState(null);
    const videoDevices = useMemoCompare(devices, (prev, next) => isEqual(prev, next));
    const [anchorEl, setAnchorEl] = useState(null);

    useEffect(() => {
        resetDeviceToIntrinsic();
    }, []);

    // Reset the video input to intrinsic if current video input is a transform device because this component
    // does not know if blur or replacement was selected. This depends on how the demo is set up.
    // TODO: use a hook in the appState to track whether blur or replacement was selected before this component mounts,
    // or maintain the state of `activeVideoTransformOption` in `MeetingManager`.
    const resetDeviceToIntrinsic = async () => {
        try {
            if (isVideoTransformDevice(selectedDevice)) {
                const intrinsicDevice = await selectedDevice.intrinsicDevice();
                await meetingManager.selectVideoInputDevice(intrinsicDevice);
            }
        } catch (error) {
            logger.error('Failed to reset Device to intrinsic device');
        }
    };

    useEffect(() => {
        const handleClick = async (deviceId) => {
            try {
                // If background blur/replacement is on, then re-use the same video transform pipeline, but replace the inner device
                // If background blur/replacement is not on, then do a normal video selection
                let newDevice = deviceId;
                if (isVideoTransformDevice(selectedDevice) && !isLoading) {
                    setIsLoading(true);
                    if ('chooseNewInnerDevice' in selectedDevice) {
                        // @ts-ignore
                        newDevice = selectedDevice.chooseNewInnerDevice(deviceId);
                    } else {
                        logger.error('Transform device cannot choose new inner device');
                        return;
                    }
                }
                if (isVideoEnabled) {
                    await meetingManager.startVideoInputDevice(newDevice);
                } else {
                    meetingManager.selectVideoInputDevice(newDevice);
                }
            } catch (error) {
                logger.error('VideoInputTransformControl failed to select video input device');
            } finally {
                setIsLoading(false);
            }
        };

        const getDropdownWithVideoTransformOptions = async () => {
            const deviceOptions = videoDevices.length ? await Promise.all(videoDevices.map(async (option) => (
                <MenuItem
                    key={option.deviceId}
                    selected={await isOptionActive(selectedDevice, option.deviceId)}
                    onClick={async () => await handleClick(option.deviceId)}
                    className={classNameForListItem}
                >
                    {option.label}
                </MenuItem>
            ))) : [{ value: 'not-available', label: notFoundMsg }].map(e =>
                <MenuItem
                    key={e}
                    className={classNameForListItem}
                >
                    {e.label}
                </MenuItem>);

            setDropdownWithVideoTransformOptions(deviceOptions);
        };

        getDropdownWithVideoTransformOptions();
    }, [
        // createBackgroundBlurDevice,
        // createBackgroundReplacementDevice,
        meetingManager,
        meetingManager.startVideoInputDevice,
        videoDevices,
        isLoading,
        isVideoEnabled,
        selectedDevice,
        // isBackgroundBlurSupported,
        // isBackgroundReplacementSupported,
    ]);

    const handleOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);

    return (
        <Grid className='transparentGrid'>
            <IconButton size="small" style={{}} onClick={handleOpen} className={iconClassName}>
                <KeyboardArrowDownIcon fontSize="25px" style={{ color: '#e95d0c' }} />
            </IconButton>
            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem className={classNameForHeader} disabled>
                    {label}
                </MenuItem>
                {dropdownWithVideoTransformOptions}
            </Menu>
        </Grid>
    );
};

export default VideoInputTransformControl;
