
import React from "react";
import { useNavigate } from 'react-router-dom';
import {
    Severity,
    ActionType,
    useNotificationDispatch,
    useAudioVideo
} from 'amazon-chime-sdk-component-library-react';
import { endMeeting } from '../../../utils/api';
import routes from '../../../constants/routes';
import { useAppState } from "../../../providers/AppStateProvider";
import { Grid, Menu, MenuItem, IconButton, Tooltip, useMediaQuery } from '@mui/material';
import CallEndIcon from '@mui/icons-material/CallEnd';
import { createTheme } from '@mui/system';

const ExitMeetingControl = () => {
    const navigate = useNavigate();
    const { meetingId } = useAppState();
    const audioVideo = useAudioVideo();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [ending, setEnding] = React.useState(false);
    const open = Boolean(anchorEl);
    const dispatch = useNotificationDispatch();
    const theme = createTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const leaveMeeting = async () => {
        // navigate(routes.HOME);
        // audioVideo.chooseVideoInputDevice(null);
        audioVideo?.stopLocalVideoTile();
        audioVideo?.stop();
        navigate(`/video_call/${meetingId}`);

    };

    const endMeetingForAll = async () => {
        try {
            if (meetingId) {
                setEnding(true);
                await endMeeting(meetingId);
                navigate(`/video_call/${meetingId}`);
            }
        } catch (error) {
            const payload = {
                severity: Severity.ERROR,
                message: (error).message,
                autoClose: true,
            };
            dispatch({
                type: ActionType.ADD,
                payload: payload,
            });
        } finally {
            setEnding(false);
        }
    };

    return (
        <Grid className='transparentGrid'>
            <Tooltip title="End Call" placement="top-end">
                <IconButton
                    onClick={handleClick}
                    size="small"
                    disabled={ending}
                    sx={{
                        // border: "1px solid #e95d0c",
                        padding: '6px',
                        // opacity: ending ? 0 : 1
                        // backgroundColor: "#e95d0c",
                        // backgroundColor: "rgb(152 151 151 / 25%)"
                    }}
                    aria-controls={open ? 'account-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? 'true' : undefined}
                >
                    <CallEndIcon
                        fontSize="medium"
                        sx={{
                            // color: '#e95d0c',
                            // color: '#fff',
                            fontSize: "23px"
                        }}
                    />
                </IconButton>
            </Tooltip>

            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        mb: "10px",
                        "&::before": {
                            bgcolor: "background.paper",
                            content: '""',
                            display: "block",
                            position: "absolute",
                            width: 12,
                            height: 12,
                            bottom: -6,
                            transform: "rotate(45deg)",
                            left: `calc(${sm ? "50%" : "75%"} - 6px)`,
                        }

                    },

                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
            >
                <MenuItem onClick={() => {
                    handleClose();
                    leaveMeeting();
                }} className="menupopOverItem">
                    <span>{"Leave Meeting"}</span>
                </MenuItem>

                <MenuItem onClick={() => {
                    handleClose();
                    endMeetingForAll();
                }} className="menupopOverItem">
                    <span>{"End meeting for all"}</span>
                </MenuItem>
            </Menu>
        </Grid>
    )
}

export default ExitMeetingControl;