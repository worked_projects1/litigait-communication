
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  useMeetingManager,
  useNotificationDispatch,
  Severity,
  ActionType,
} from 'amazon-chime-sdk-component-library-react';
import routes from '../constants/routes';

const NoMeetingRedirect = ({ children }) => {
  const navigate = useNavigate();
  const dispatch = useNotificationDispatch();
  const meetingManager = useMeetingManager();

  const payload = {
    severity: Severity.INFO,
    message: 'No meeting found, please enter a valid meeting Id',
    autoClose: true,
  };

  useEffect(() => {
    console.log("meetingManager2 = ", meetingManager);
    if (!meetingManager.meetingSession) {
      dispatch({
        type: ActionType.ADD,
        payload: payload,
      });
      navigate(routes.HOME);
    }
  }, [meetingManager]);

  return <>{children}</>;
};

export default NoMeetingRedirect;
