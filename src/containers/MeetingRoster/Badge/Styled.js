
import styled from 'styled-components';

export const StyledBadge = styled.span`
  ${(props) => {
    if (typeof props.value === 'object') {
      const element = props.value;
      const width = (element.props && element.props.width) || '1rem';
      return `width: ${width};`;
    }
    return null;
  }}
  display: inline-block;
  // padding: ${(props) =>
    typeof props.value === 'object' ? '0' : '0.1rem 0.4rem 0.125rem'};
  padding: ${(props) =>
    typeof props.value === 'object' ? '0' : '4px 6px 1px 6px'};
  border-radius: 0.5rem;
  line-height: ${(props) => (typeof props.value === 'object' ? '1' : '1.43')};
  color: ${(props) => props.theme.colors.greys.white};
  font-size: 0.65rem;
  background-color: ${(props) =>
    props.status === 'alert'
      ? props.theme.colors.error.primary
      : props.theme.colors.greys.grey60};

`;
