
import React from 'react';
import { StyledBadge } from './Styled';

export const Badge = ({
    value,
    status = 'default',
    className,
    tag,
    ...rest
}) => {
    return (
        <StyledBadge
            className={className || ''}
            as={tag}
            status={status}
            value={value}
            data-testid="badge"
            {...rest}
        >
            {value}
        </StyledBadge>
    );
};

export default Badge;
