
import React, { useEffect, useRef } from 'react';
import { useAttendeeAudioStatus, useAudioVideo } from 'amazon-chime-sdk-component-library-react';
import MicVolumeIndicator from '../MicVolumeIndicator';

export const MicrophoneActivity = ({
    attendeeId,
    ...rest
}) => {
    const audioVideo = useAudioVideo();
    const bgEl = useRef(null);
    const { signalStrength, muted } = useAttendeeAudioStatus(attendeeId);

    useEffect(() => {
        if (!audioVideo || !attendeeId || !bgEl.current) {
            return;
        }

        const callback = (
            _,
            volume,
            __,
            ___
        ) => {
            if (bgEl.current) {
                bgEl.current.style.transform = `scaleY(${volume})`;
            }
        };

        audioVideo.realtimeSubscribeToVolumeIndicator(attendeeId, callback);

        return () =>
            audioVideo.realtimeUnsubscribeFromVolumeIndicator(attendeeId, callback);
    }, [attendeeId]);

    return (
        <MicVolumeIndicator
            {...rest}
            ref={bgEl}
            muted={muted}
            signalStrength={signalStrength}
        />
    );
};

export default MicrophoneActivity;
