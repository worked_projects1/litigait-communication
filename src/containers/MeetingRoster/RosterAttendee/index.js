
import React from 'react';
import { useAttendeeStatus, useRosterState } from 'amazon-chime-sdk-component-library-react';
import RosterCell from '../RosterCell';
import MicrophoneActivity from '../MicrophoneActivity';

export const RosterAttendee = ({
    attendeeId,
    ...rest
}) => {
    const { muted, videoEnabled, sharingContent } = useAttendeeStatus(attendeeId);
    const { roster } = useRosterState();
    const attendeeName = roster[attendeeId]?.name || '';

    return (
        <RosterCell
            name={attendeeName}
            muted={muted}
            videoEnabled={videoEnabled}
            sharingContent={sharingContent}
            microphone={<MicrophoneActivity attendeeId={attendeeId} />}
            {...rest}
        />
    );
};

export default RosterAttendee;
