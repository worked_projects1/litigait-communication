
import React from 'react';
import { IconButton, Dots, PopOver } from 'amazon-chime-sdk-component-library-react';

export const PopOverMenu = ({
    menu,
    buttonProps,
    tooltipContainerId,
    a11yMenuLabel = '',
    ...rest
}) => {

    const ButtonComponent = IconButton;
    const buttonComponentProps = rest['data-tooltip-position']
        ? { tooltipPosition: rest['data-tooltip-position'] }
        : {};
    return (
        <PopOver
            className="ch-menu"
            a11yLabel={a11yMenuLabel}
            renderButtonWrapper={(isActive, props) => (
                <ButtonComponent
                    {...buttonComponentProps}
                    {...buttonProps}
                    {...props}
                    {...rest}
                    className={`ch-menu- ${buttonProps?.className}`}
                    icon={<Dots />}
                    label={a11yMenuLabel}
                />
            )}
        >
            {menu}
        </PopOver>
    );
};
