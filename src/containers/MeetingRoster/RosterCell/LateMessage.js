
import React from 'react';
import { Clock } from 'amazon-chime-sdk-component-library-react';
import { StyledLateMessage } from './Styled';

const LateMessage = ({ children }) => (
    <StyledLateMessage>
        <Clock />
        {children}
    </StyledLateMessage>
);

export default LateMessage;
