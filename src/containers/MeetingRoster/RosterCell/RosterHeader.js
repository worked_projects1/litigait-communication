
import React, {
    useEffect,
    useRef,
    useState,
} from 'react';

import Badge from '../Badge';
import { PopOverMenu } from './PopOverMenu';
import { StyledHeader } from './Styled';
import { Grid, IconButton, Autocomplete, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

const SearchBar = ({ onChange, onClose, value, options }) => {
    const inputEl = useRef(null);

    useEffect(() => {
        inputEl.current?.focus();
    }, []);

    return (
        <Grid container alignItems="center" className="ch-search-wrapper">
            <Autocomplete
                id="free-solo-demo"
                freeSolo
                options={options.map((option) => option.name)}
                // style={{
                //     // width: '90%',
                //     flex: 1
                // }}
                sx={{
                    flex: 1,
                    "& .MuiOutlinedInput-root": {
                        borderColor: "#e95d0c",
                        borderWidth: 1,
                        padding: "0",
                        "& .MuiAutocomplete-input": {
                            padding: "4px 2px"
                        }
                    },
                    "& .MuiAutocomplete-input": {
                        padding: "0"
                    },
                    "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
                        border: "1px solid #e95d0c"
                    },
                    "& .MuiAutocomplete-endAdornment": {
                        scale: "0.5"
                    }
                }}
                renderInput={(params) => <TextField {...params} label="" />}
            />
            <IconButton
                onClick={onClose}
                sx={{ padding: '2px', marginLeft: '8px' }}
            >
                <HighlightOffIcon
                    // style={{ color: '#e95d0c', fontSize: "20px", }}
                    sx={{
                        // color: '#e95d0c',
                        // color: '#fff',
                        fontSize: "16px"
                    }}
                />
            </IconButton>
        </Grid>
    );
};

export const RosterHeader = ({
    tag,
    title,
    badge,
    searchValue,
    onClose,
    onSearch,
    className,
    menu,
    a11yMenuLabel = '',
    searchLabel = 'Open search',
    closeLabel = 'Close',
    tooltipContainerId,
    children,
    options,
    ...rest
}) => {

    const buttonComponentProps = rest['data-tooltip-position']
        ? { tooltipPosition: rest['data-tooltip-position'] }
        : {};
    const popOverMenuComponentProps = rest['data-tooltip']
        ? {
            ['data-tooltip-position']: rest['data-tooltip-position'],
            ['data-tooltip']: rest['data-tooltip'],
        }
        : {};
    const [isSearching, setIsSearching] = useState(false);
    const searchBtn = useRef(null);

    const openSearch = () => {
        setIsSearching(true);
    };

    const closeSearch = () => {
        onSearch?.({
            target: {
                value: '',
            },
            currentTarget: {
                value: '',
            },
        });
        setIsSearching(false);
        searchBtn.current?.focus();
    };

    return (
        <Grid
            className={className || ''}
        >
            <StyledHeader
                isSearching={isSearching}
                as={tag}

                {...rest}
            >
                <div className="ch-title">{title}</div>
                {typeof badge === 'number' && badge > -1 && (
                    <Badge className="ch-badge" value={badge} />
                )}
                <div className="ch-buttons">
                    {onSearch && (<IconButton
                        ref={searchBtn}
                        label={searchLabel}
                        onClick={openSearch}
                        {...buttonComponentProps}
                        sx={{
                            // border: "1px solid #e95d0c",
                            padding: '2px',
                            // backgroundColor: "#e95d0c",
                            // backgroundColor: "rgb(152 151 151 / 25%)"
                        }}
                    >
                        <SearchIcon
                            // style={{ color: '#e95d0c', fontSize: "20px" }}
                            sx={{
                                // color: '#e95d0c',
                                // color: '#fff',
                                fontSize: "16px"
                            }}
                        />
                    </IconButton>)}
                    {menu && (
                        <PopOverMenu
                            {...popOverMenuComponentProps}
                            tooltipContainerId={tooltipContainerId}
                            menu={menu}
                            a11yMenuLabel={a11yMenuLabel}
                        />
                    )}
                    {children}
                    {onClose && (
                        <IconButton
                            {...buttonComponentProps}
                            label={closeLabel}
                            onClick={onClose}
                            sx={{
                                // border: "1px solid #e95d0c",
                                padding: '2px',
                                // backgroundColor: "#e95d0c",
                                // backgroundColor: "rgb(152 151 151 / 25%)"
                            }}
                            size="small"
                        >
                            <CloseIcon
                                // style={{ color: '#e95d0c', fontSize: "20px" }}
                                sx={{
                                    // color: '#e95d0c',
                                    // color: '#fff',
                                    fontSize: "16px"
                                }}
                            />
                        </IconButton>
                    )}

                </div>
                {isSearching && (
                    <SearchBar
                        value={searchValue}
                        onClose={closeSearch}
                        onChange={onSearch}
                        options={options}
                    />
                )}
            </StyledHeader>
        </Grid>
    );
};

export default RosterHeader;
