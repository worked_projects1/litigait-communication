
import styled from 'styled-components';

export const StyledCell = styled.div`
  display: flex;
  align-items: center;
  padding: 0.625rem 1rem;

  .ch-mic {
    flex-shrink: 0;
    width: 1.5rem;
    line-height: 0;

    ${({ micPosition }) =>
    micPosition === 'leading'
      ? `
        order: -1;
        margin-right: .75rem;
      `
      : ''}
  }

  .ch-menu {
    color: ${(props) => props.theme.buttons.icon.hover.bgd};

    &:hover,
    &:focus {
      color: ${(props) => props.theme.buttons.icon.hover.text};
    }
  }

  svg {
    width: 1.5rem;
    flex-shrink: 0;
  }

  > * {
    margin-right: 0.5rem;
  }

  > :last-child {
    margin-right: 0;
  }

`;

export const StyledLateMessage = styled.div`
  display: flex;
  align-items: center;
  white-space: nowrap;
  font-size: 0.65rem;
  color: ${({ theme }) => theme.roster.secondaryText};

  > svg {
    margin-right: 0.25rem;
    color: ${({ theme }) => theme.roster.secondaryText};
  }
`;

export const StyledRoster = styled.aside`
  width: 100%;
  height: 100%;
  padding-bottom: 1rem;
  overflow-y: auto;
  background-color: ${(props) => props.theme.roster.bgd};
  box-shadow: 1rem 1rem 3.75rem 0 rgba(0, 0, 0, 0.1);
  border-left: 0.0625rem solid ${(props) => props.theme.roster.containerBorder};
  border-right: 0.0625rem solid ${(props) => props.theme.roster.containerBorder};

  ${({ theme }) => theme.mediaQueries.min.md} {
    width: ${(props) => props.theme.roster.maxWidth};
  }

`;

export const StyledTitle = styled.span`
  display: inline-block;
  margin: 0 0.625rem 0 0;
  font-weight: 600;
  font-size: 0.675rem;
  color: ${(props) => props.theme.roster.secondaryText};
`;

export const StyledGroupWrapper = styled.div`
  margin: 0 0.5rem;

  & + & {
    margin-top: 1rem;
  }

`;

export const StyledGroup = styled.div`
  background-color: ${(props) => props.theme.roster.fgd};
  border-radius: ${(props) => props.theme.radii.default};

`;

export const StyledHeader = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding: 0.75rem ${(props) => props.isSearching ? '0rem' : '1rem'};
  margin-bottom: 0.5rem;
  border-bottom: 0.0625rem solid ${(props) => props.theme.roster.headerBorder};

  .ch-title {
    font-size: 0.875rem;
    color: ${(props) => props.theme.roster.primaryText};
    ${(props) => (props.isSearching ? 'display: none;' : '')}
  }

  .ch-badge {
    margin-left: 0.5rem;
    ${(props) => (props.isSearching ? 'display: none;' : '')}
    font-size: 12px;
    // padding-top: 4px;
    border-radius: 50%;
  }

  .ch-buttons {
    margin-left: auto;
    display: flex;

    > * {
      margin-left: 0.5rem;
    }

    ${(props) => (props.isSearching ? 'display: none;' : '')}
  }

  .ch-search-wrapper {
    margin: 0px 0.5rem;
    .ch-search-input {
      flex: 1;

      input {
        width: 100%;
      }
    }

    .ch-search-close {
      margin-left: 0.5rem;
    }
  }

  .ch-navigation-icon {
    margin-right: 0.5rem;
    margin-left: -0.5rem;

    ${({ theme }) => theme.mediaQueries.min.md} {
      display: none;
    }
  }

`;

export const StyledName = styled.div`
  flex-grow: 1;
  min-width: 0;
  line-height: 1.5;

  .ch-name {
    font-size: 0.875rem;
    color: ${(props) => props.theme.roster.primaryText};
  }

  .ch-subtitle {
    font-size: 0.65rem;
    color: ${(props) => props.theme.roster.secondaryText};
  }
`;
