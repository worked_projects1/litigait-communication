
import React from 'react';
import { Camera, Microphone, ScreenShare } from 'amazon-chime-sdk-component-library-react';
import { PopOverMenu } from './PopOverMenu';
import LateMessage from './LateMessage';
import { StyledCell, StyledName } from './Styled';

const RosterName = ({ name, subtitle }) => (
    <StyledName>
        <div className="ch-name">{name}</div>
        {subtitle && <div className="ch-subtitle">{subtitle}</div>}
    </StyledName>
);

function getVideoIcon(
    isVideoEnabled,
    isSharingContent
) {
    if (isSharingContent) {
        return <ScreenShare />;
    }

    if (typeof isVideoEnabled === 'boolean') {
        return <Camera disabled={!isVideoEnabled} />;
    }

    return null;
}

export const RosterCell = (props) => {
    const {
        tag,
        name,
        menu,
        subtitle,
        className,
        runningLate,
        muted,
        videoEnabled,
        sharingContent,
        poorConnection = false,
        microphone,
        a11yMenuLabel = '',
        extraIcon,
        buttonProps,
        ...rest
    } = props;

    const videoIcon = getVideoIcon(videoEnabled, sharingContent);
    const showMic = typeof muted === 'boolean';
    const mic = microphone || (
        <Microphone muted={muted} poorConnection={poorConnection} />
    );

    const popOverMenuComponentProps = rest['data-tooltip']
        ? {
            ['data-tooltip-position']: rest['data-tooltip-position'],
            ['data-tooltip']: rest['data-tooltip'],
        }
        : {};

    return (
        <StyledCell
            className={className || ''}
            as={tag}
            {...props}
            data-testid="roster-cell"
        >
            <RosterName name={name} subtitle={subtitle} />
            {runningLate ? (
                <LateMessage>{runningLate}</LateMessage>
            ) : (
                <>
                    {showMic && <div className="ch-mic">{mic}</div>}
                    {extraIcon}
                    {videoIcon}
                </>
            )}
            {menu && (
                <PopOverMenu
                    {...popOverMenuComponentProps}
                    menu={menu}
                    a11yMenuLabel={a11yMenuLabel}
                    buttonProps={buttonProps}
                />
            )}
        </StyledCell>
    );
};

export default RosterCell;
