
import React from 'react';
import Badge from '../Badge';
import { Flex } from 'amazon-chime-sdk-component-library-react';
import { StyledGroup, StyledGroupWrapper, StyledTitle } from './Styled';

export const RosterGroup = ({
    tag,
    title,
    badge,
    className,
    children,
    ...rest
}) => {
    return (
        <StyledGroupWrapper as={tag} className={className || ''} {...rest}>
            {title && (
                <Flex alignItems="center" pl=".5rem" mb=".5rem">
                    <StyledTitle>{title}</StyledTitle>
                    {typeof badge === 'number' && badge > -1 && <Badge value={badge} />}
                </Flex>
            )}
            <StyledGroup>{children}</StyledGroup>
        </StyledGroupWrapper>
    );
};

export default RosterGroup;
