
import React, { useState } from 'react';
import {
    useRosterState,
} from 'amazon-chime-sdk-component-library-react';
import RosterAttendeeWrapper from './RosterAttendeeWrapper';
// import './styles.css';
import RosterGroup from './RosterCell/RosterGroup';
import { RosterHeader } from './RosterCell/RosterHeader';
import { Grid } from '@mui/material';
import useStyles from './styles';

const MeetingRoster = (props) => {
    const { navigationControl } = props;
    const { roster } = useRosterState();
    const [filter, setFilter] = useState('');
    const { closeRoster } = navigationControl;
    const { classes } = useStyles();

    let attendees = Object.values(roster);

    if (filter) {
        attendees = attendees.filter((attendee) =>
            attendee?.name?.toLowerCase().includes(filter.trim().toLowerCase())
        );
    }

    const handleSearch = (e) => {
        setFilter(e.target.value);
    };

    const attendeeItems = attendees.map((attendee) => {
        const { chimeAttendeeId } = attendee || {};
        return <RosterAttendeeWrapper key={chimeAttendeeId} attendeeId={chimeAttendeeId} />;
    });

    return (
        <Grid
            // className='roster'
            className={classes.roster}
        >
            <RosterHeader
                searchValue={filter}
                onSearch={handleSearch}
                onClose={closeRoster}
                title="Present"
                badge={attendees.length}
                options={attendees}
                // className="rosterHeader"
                className={classes.rosterHeader}
            />
            <RosterGroup name="group">{attendeeItems}</RosterGroup>
        </Grid>
    );
};

export default MeetingRoster;
