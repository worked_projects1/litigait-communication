
import React from 'react';
import RosterAttendee from './RosterAttendee';

const RosterAttendeeWrapper = ({ attendeeId }) => {
    return (
        <RosterAttendee
            attendeeId={attendeeId}
        />
    );
};

export default RosterAttendeeWrapper;
