
import React, { forwardRef } from 'react';
import { Microphone } from 'amazon-chime-sdk-component-library-react';
import { StyledMicVolumeIndicator } from './Styled';

export const MicVolumeIndicator = forwardRef(
    (
        {
            muted = false,
            signalStrength,
            className: propClassName,
            ...rest
        },
        bgRef
    ) => {
        const poorConnection =
            signalStrength !== undefined && signalStrength <= 0.5;
        const className = propClassName
            ? `${propClassName} ch-mic-volume-indicator`
            : 'ch-mic-volume-indicator';

        return (
            <StyledMicVolumeIndicator
                className={className}
                signalStrength={signalStrength}
                muted={muted}
                {...rest}
            >
                <Microphone
                    muted={muted}
                    className="ch-mic-icon"
                    poorConnection={poorConnection}
                />
                <div className="ch-bg-volume-wrapper">
                    <div
                        ref={bgRef}
                        className="ch-bg-volume-fill"
                        data-testid="volume-fill"
                    />
                </div>
            </StyledMicVolumeIndicator>
        );
    }
);

export default MicVolumeIndicator;
