import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    roster: {
        width: "18.5rem",
        borderRadius: "5px",
        padding: "8px",
        marginBottom: "0px",
        fontFamily: 'Avenir-Bold',
        height: "100%",
        "@media(max-width: 35.5rem)": {
            width: "100%"
        }
    },
    rosterHeader: {
        height: "8%"
    }
})
);

export default useStyles;