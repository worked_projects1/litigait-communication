import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    backGroundSettingDiv: {
        width: "18.5rem",
        borderRadius: "5px",
        padding: "8px",
        marginBottom: "0px",
        fontFamily: 'Avenir-Bold',
        height: "100%",
        // backgroundColor: "#fff",
        '@media(max-width: 35.5rem)': {
            width: "100%"
        },
    },
    backGroundHeader: {
        fontFamily: 'Avenir-Bold',
    },
    imageOverAllDiv: {
        justifyContent: "space-evenly",
        overflow: "auto",
        height: "90%"
    },
    backGroundHeaderDiv: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
    },
    backgroundDivItem: {
        marginTop: "10px"
    },
    backgroundNoneItem: {
        marginTop: "10px",
        marginBottom: "10px"
    },
    backgroundContainerItem: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        boxShadow: "lightgrey 0px 0px 8px 1px",
        cursor: "pointer",
        minHeight: "100px"
    },
    imageDiv: {
        height: "75%",
        display: "flex",
        alignItems: "center",
        position: "relative"
    },
    textDiv: {
        height: "25%"
    },
    blurDiv: {
        height: "75%",
        display: "flex",
        alignItems: "center",
        position: "relative"
    },
    progressDiv: {
        position: "absolute",
        /* width: '100%' */
    },
    progressDivImage: {
        position: "absolute",
        left: "40%"
    }
})
);

export default useStyles;