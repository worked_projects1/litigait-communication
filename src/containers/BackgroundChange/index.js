import React, { useEffect, useState } from "react";
import { CircularProgress, Grid, Typography, IconButton } from "@mui/material";
// import './styles.css';
import styled from "styled-components";
// import PanoramaIcon from '@mui/icons-material/Panorama';
// import WallpaperIcon from '@mui/icons-material/Wallpaper';
// import FilterHdrIcon from '@mui/icons-material/FilterHdr';
// import PhotoFilterIcon from '@mui/icons-material/PhotoFilter';
import AbstractThumb from '../../images/videobackgrounds/thumb/Abstract.jpg';
import Abstract from '../../images/videobackgrounds/Abstract.jpg';
import BohoHomeThumb from '../../images/videobackgrounds/thumb/BohoHome.jpg';
import BohoHome from '../../images/videobackgrounds/BohoHome.jpg';
import BookshelfThumb from '../../images/videobackgrounds/thumb/Bookshelf.jpg';
import Bookshelf from '../../images/videobackgrounds/Bookshelf.jpg';
import CoffeeShopThumb from '../../images/videobackgrounds/thumb/CoffeeShop.jpg';
import CoffeeShop from '../../images/videobackgrounds/CoffeeShop.jpg';
import ContemporaryThumb from '../../images/videobackgrounds/thumb/Contemporary.jpg';
import Contemporary from '../../images/videobackgrounds/Contemporary.jpg';
import CozyHomeThumb from '../../images/videobackgrounds/thumb/CozyHome.jpg';
import CozyHome from '../../images/videobackgrounds/CozyHome.jpg';
import DesertThumb from '../../images/videobackgrounds/thumb/Desert.jpg';
import Desert from '../../images/videobackgrounds/Desert.jpg';
import FishingThumb from '../../images/videobackgrounds/thumb/Fishing.jpg';
import Fishing from '../../images/videobackgrounds/Fishing.jpg';
import FlowerThumb from '../../images/videobackgrounds/thumb/Flower.jpg';
import Flower from '../../images/videobackgrounds/Flower.jpg';
import KitchenThumb from '../../images/videobackgrounds/thumb/Kitchen.jpg';
import Kitchen from '../../images/videobackgrounds/Kitchen.jpg';
import ModernHomeThumb from '../../images/videobackgrounds/thumb/ModernHome.jpg';
import ModernHome from '../../images/videobackgrounds/ModernHome.jpg';
import NatureThumb from '../../images/videobackgrounds/thumb/Nature.jpg';
import Nature from '../../images/videobackgrounds/Nature.jpg';
import OceanThumb from '../../images/videobackgrounds/thumb/Ocean.jpg';
import Ocean from '../../images/videobackgrounds/Ocean.jpg';
import PatioThumb from '../../images/videobackgrounds/thumb/Patio.jpg';
import Patio from '../../images/videobackgrounds/Patio.jpg';
import PlantThumb from '../../images/videobackgrounds/thumb/Plant.jpg';
import Plant from '../../images/videobackgrounds/Plant.jpg';
import SanFranciscoThumb from '../../images/videobackgrounds/thumb/SanFrancisco.jpg';
import SanFrancisco from '../../images/videobackgrounds/SanFrancisco.jpg';
// import CreativeThumb from '../../images/videobackgrounds/Creative.jpg';
import Creative from '../../images/videobackgrounds/Creative.jpg';

import BlurOnOutlinedIcon from '@mui/icons-material/BlurOnOutlined';
import NotInterestedOutlinedIcon from '@mui/icons-material/NotInterestedOutlined';
// import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import CloseIcon from '@mui/icons-material/Close';
import {
    // Device,
    isVideoTransformDevice,
    // VideoFxProcessor,
    // BackgroundReplacementVideoFrameProcessor
} from 'amazon-chime-sdk-js';
import {
    useBackgroundBlur,
    useBackgroundReplacement,
    // FormField,
    // Select,
    useVideoInputs,
    useMeetingManager,
    useLocalVideo,
    useLogger
} from 'amazon-chime-sdk-component-library-react';
// import { useAppState } from "../../providers/AppStateProvider";
// import { ClassNames } from "@emotion/react";
import useStyles from './styles';

const BackgroundChangeHeader = styled(Grid)`
padding: 20px;
border-bottom: 0.0625rem solid ${(props) => props.theme.roster.containerBorder};
height: 10%
`;

const BackgroundChange = (props) => {
    const { navigationControl } = props;
    const { setImageBlob, imageBlob, toggleBackgroundChange } = navigationControl;
    const meetingManager = useMeetingManager();
    const logger = useLogger();
    const {
        // devices,
        selectedDevice
    } = useVideoInputs();
    const {
        isVideoEnabled,
        // toggleVideo
    } = useLocalVideo();
    const {
        // isBackgroundBlurSupported,
        createBackgroundBlurDevice
    } = useBackgroundBlur();
    const {
        // isBackgroundReplacementSupported,
        createBackgroundReplacementDevice
    } = useBackgroundReplacement();
    const [isLoading, setIsLoading] = useState(false);
    const [imageData, setImageData] = useState(imageBlob);
    const [selectedmode, setMode] = useState('reset');
    const [imageName, selectImage] = useState('');
    const { classes } = useStyles();

    useEffect(() => {
        const createBackground = async () => {
            let current = selectedDevice;
            if (current === undefined) {
                return;
            }

            try {
                setIsLoading(true);
                if (!isVideoTransformDevice(current) && selectedmode === "backgroundReplacement") {
                    // setImageData(imageBlob);
                    // Enable video transform on the non-transformed device.
                    current = await createBackgroundReplacementDevice(current);
                    logger.info(`Video filter turned on - selecting video transform device: ${JSON.stringify(current)}`);
                } else if (!isVideoTransformDevice(current) && selectedmode === "backgroundBlur") {
                    // Enable video transform on the non-transformed device.
                    current = await createBackgroundBlurDevice(current);
                    logger.info(`Video filter turned on - selecting video transform device: ${JSON.stringify(current)}`);
                }
                // else {
                //     // Switch back to intrinsicDevice.
                //     const intrinsicDevice = await current.intrinsicDevice();
                //     // Stop existing VideoTransformDevice.
                //     await current.stop();
                //     current = intrinsicDevice;
                // }

                if (isVideoEnabled) {
                    // Use the new created video device as input.
                    await meetingManager.startVideoInputDevice(current);
                } else {
                    // Select the new created video device but don't start it.
                    await meetingManager.selectVideoInputDevice(current);
                }

            } catch (e) {
                logger.error(`Error trying to toggle background replacement ${e}`);
            } finally {
                setIsLoading(false);
            }
        }
        createBackground();
    }, [imageData, selectedmode])

    const toggleReplaceBackground = async (imageUrl, mode) => {
        let current = selectedDevice;
        if (isLoading || current === undefined) {
            return;
        }
        console.log("enteredherelike = ", mode, "imageUrl = ", imageUrl, "isVideoTransformDevice = ", !isVideoTransformDevice(current));
        try {
            selectImage(imageUrl);
            setIsLoading(true);
            if (!isVideoTransformDevice(current) && mode === "backgroundReplacement") {
                setMode('backgroundReplacement');
                const res = await fetch(imageUrl);
                const blobData = await res.blob();

                setImageData(blobData);
                setImageBlob(blobData);
                // Enable video transform on the non-transformed device.
                // current = await createBackgroundReplacementDevice(current);
                // logger.info(`Video filter turned on - selecting video transform device: ${JSON.stringify(current)}`);
            } else if (!isVideoTransformDevice(current) && mode === "backgroundBlur" && selectedmode !== "backgroundBlur") {
                setMode('backgroundBlur');
                // Enable video transform on the non-transformed device.
                // current = await createBackgroundBlurDevice(current);
                logger.info(`Video filter turned on - selecting video transform device: ${JSON.stringify(current)}`);
            } else if (isVideoTransformDevice(current)) {
                // Switch back to intrinsicDevice.
                const intrinsicDevice = await current.intrinsicDevice();
                // Stop existing VideoTransformDevice.
                await current.stop();
                current = intrinsicDevice;
            }
            else {
                setMode('reset');
                //     // Switch back to intrinsicDevice.
                //     const intrinsicDevice = await current.intrinsicDevice();
                //     // Stop existing VideoTransformDevice.
                //     await current.stop();
                //     current = intrinsicDevice;
            }

            // if (isVideoEnabled) {
            //     // Use the new created video device as input.
            //     await meetingManager.startVideoInputDevice(current);
            // } else {
            //     // Select the new created video device but don't start it.
            //     await meetingManager.selectVideoInputDevice(current);
            // }

        } catch (e) {
            logger.error(`Error trying to toggle background replacement ${e}`);
        } finally {
            // setIsLoading(false);
        }
    };

    // const resetDeviceToIntrinsic = async() => {
    //     // Switch back to intrinsicDevice.
    //     const intrinsicDevice = await current.intrinsicDevice();
    //     // Stop existing VideoTransformDevice.
    //     await current.stop();
    //     current = intrinsicDevice;
    //     if (isVideoEnabled) {
    //         // Use the new created video device as input.
    //         await meetingManager.startVideoInputDevice(current);
    //     } else {
    //         // Select the new created video device but don't start it.
    //         await meetingManager.selectVideoInputDevice(current);
    //     }
    // }

    const images = [
        {
            thumb: AbstractThumb,
            name: "Abstract",
            imageUrl: Abstract
        },
        {
            thumb: BohoHomeThumb,
            name: "Boho Home",
            imageUrl: BohoHome
        },
        {
            thumb: BookshelfThumb,
            name: "Bookshelf",
            imageUrl: Bookshelf
        },
        {
            thumb: CoffeeShopThumb,
            name: "Coffee Shop",
            imageUrl: CoffeeShop
        },
        {
            thumb: ContemporaryThumb,
            name: "Contemporary",
            imageUrl: Contemporary
        },
        {
            thumb: CozyHomeThumb,
            name: "Cozy Home",
            imageUrl: CozyHome
        },
        {
            thumb: DesertThumb,
            name: "Desert",
            imageUrl: Desert
        },
        {
            thumb: FishingThumb,
            name: "Fishing",
            imageUrl: Fishing
        },
        {
            thumb: FlowerThumb,
            name: "Flower",
            imageUrl: Flower
        },
        {
            thumb: KitchenThumb,
            name: "Kitchen",
            imageUrl: Kitchen
        },
        {
            thumb: ModernHomeThumb,
            name: "Modern Home",
            imageUrl: ModernHome
        },
        {
            thumb: NatureThumb,
            name: "Nature",
            imageUrl: Nature
        },
        {
            thumb: OceanThumb,
            name: "Ocean",
            imageUrl: Ocean
        },
        {
            thumb: PatioThumb,
            name: "Patio",
            imageUrl: Patio
        },
        {
            thumb: PlantThumb,
            name: "Plant",
            imageUrl: Plant
        },
        {
            thumb: SanFranciscoThumb,
            name: "San Francisco",
            imageUrl: SanFrancisco
        },
        {
            thumb: Creative,
            name: "Creative",
            imageUrl: Creative
        },
    ]

    return (
        <Grid
            // className="backGroundSettingDiv"
            className={classes.backGroundSettingDiv}
        >
            <BackgroundChangeHeader
                // className="backGroundHeaderDiv"
                className={classes.backGroundHeaderDiv}
            >
                <Typography
                    variant="subtitle2"
                    // className="backGroundHeader"
                    className={classes.backGroundHeader}
                >
                    Background Change
                </Typography>
                <IconButton
                    onClick={() => toggleBackgroundChange()}
                    sx={{ padding: '2px', marginLeft: '8px' }}
                    size="small"
                >
                    <CloseIcon
                        // style={{ color: '#e95d0c', fontSize: "20px", }}
                        sx={{
                            // color: '#e95d0c',
                            // color: '#fff',
                            fontSize: "18px"
                        }}
                    />
                </IconButton>
            </BackgroundChangeHeader>

            <Grid
                container
                // className="imageOverAllDiv"
                className={classes.imageOverAllDiv}
            // style={{ justifyContent: 'space-evenly', overflow: "auto", height: '86%' }}
            >
                {images.map((image, i) => {
                    return (<Grid
                        item
                        xs={5}
                        // className="backgroundDivItem"
                        className={classes.backgroundDivItem}
                        onClick={() => toggleReplaceBackground(image.imageUrl, 'backgroundReplacement')}
                    >
                        <Grid
                            container
                            // className="backgroundContainerItem"
                            className={classes.backgroundContainerItem}
                        >
                            <Grid
                                // className="imageDiv"
                                className={classes.imageDiv}
                            >
                                {/* {isLoading && imageName == image.imageUrl && selectedmode == "backgroundReplacement" ? <CircularProgress /> : */}
                                <img src={image.thumb} alt={image.name} style={{ width: '100%', height: '100%' }} />
                                {/* } */}
                                {isLoading && imageName === image.imageUrl && selectedmode === "backgroundReplacement" ?
                                    <Grid
                                        // className="progressDivImage"
                                        className={classes.progressDivImage}
                                    >
                                        <CircularProgress size={20} />
                                    </Grid>
                                    : null
                                }
                            </Grid>
                            <Grid
                                // className="textDiv"
                                className={classes.textDiv}
                                style={{
                                    padding: '5px'
                                }}>
                                <span
                                    style={{
                                        fontFamily: "Avenir-Bold !important",
                                        fontSize: '14px'
                                    }}>{image.name}</span>
                            </Grid>
                        </Grid>
                    </Grid>)
                })}
                <Grid item xs={5} style={{
                }}
                    // className="backgroundDivItem"
                    className={classes.backgroundDivItem}
                    onClick={() => toggleReplaceBackground({}, 'backgroundBlur')}
                >
                    <Grid
                        // className="backgroundContainerItem"
                        className={classes.backgroundContainerItem}
                        container
                    >
                        <Grid
                            // className="blurDiv"
                            className={classes.blurDiv}
                        >
                            {/* {isLoading && selectedmode == "backgroundBlur" ? <CircularProgress /> : */}<BlurOnOutlinedIcon />
                            {/* } */}
                            {isLoading && selectedmode === "backgroundBlur" ?
                                <Grid
                                    // className="progressDiv"
                                    className={classes.progressDiv}
                                >
                                    <CircularProgress size={20} />
                                </Grid>
                                : null
                            }
                        </Grid>
                        <Grid
                            // className="textDiv"
                            className={classes.textDiv}
                            style={{
                                padding: '5px'
                            }}>
                            <span
                                style={{
                                    fontFamily: "Avenir-Bold !important",
                                    fontSize: '14px'
                                }}>Blur</span>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid
                    item
                    xs={5}
                    // className="backgroundNoneItem"
                    className={classes.backgroundNoneItem}
                    onClick={() => toggleReplaceBackground({}, "reset")}
                >
                    <Grid
                        container
                        // className="backgroundContainerItem"
                        className={classes.backgroundContainerItem}
                    >
                        <Grid
                            // className="blurDiv"
                            className={classes.blurDiv}
                        >
                            {/* <Grid> */}
                            {/* {isLoading && selectedmode == "reset" ? <CircularProgress /> : */}
                            <NotInterestedOutlinedIcon />
                            {/* // } */}

                            {isLoading && selectedmode === "reset" ?
                                <Grid
                                    // className="progressDiv"
                                    className={classes.progressDiv}
                                >
                                    <CircularProgress size={20} />
                                </Grid>
                                : null
                            }
                        </Grid>

                        <Grid
                            // className="textDiv"
                            className={classes.textDiv}
                            style={{
                                padding: '5px'
                            }}>
                            <span
                                style={{
                                    fontFamily: "Avenir-Bold !important",
                                    fontSize: '14px'
                                }}>None</span>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default BackgroundChange;