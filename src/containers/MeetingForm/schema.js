
export default function () {
    function videoCallForm() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'title',
                    label: 'Meeting Title',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'Paritcipant Name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'input',
                },
            ]
        }
    }

    return {
        videoCallForm
    }
}

