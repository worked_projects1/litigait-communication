
import React, { useContext, useState, useEffect, memo } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  DeviceLabels,
  Modal,
  ModalBody,
  ModalHeader,
  useMeetingManager,
  useLogger,
  Severity,
  ActionType,
  useNotificationDispatch
} from 'amazon-chime-sdk-component-library-react';
import { DefaultBrowserBehavior, MeetingSessionConfiguration, VideoPriorityBasedPolicy, VideoAdaptiveProbePolicy } from 'amazon-chime-sdk-js';
import { getErrorContext } from '../../providers/ErrorProvider';
import routes from '../../constants/routes';
import Card from '../../components/Card';
import DevicePermissionPrompt from '../DevicePermissionPrompt';
import { fetchMeeting, getAttendee } from '../../utils/api';
import { useAppState } from '../../providers/AppStateProvider';
import meetingConfig from '../../meetingConfig';
import { Grid, Typography, Button } from '@mui/material';
import EditRecordForm from '../../components/EditRecordForm';
import useStyles from './styles';
import schema from './schema';
import LoadingButton from '@mui/lab/LoadingButton';


const videoCallForm = schema().videoCallForm().columns;

const MeetingForm = () => {
  const meetingManager = useMeetingManager();
  const {
    region,
    meetingId,
    setJoinInfo,
    isEchoReductionEnabled,
    setMeetingId,
    setLocalUserName,
    setRegion,
    setLocalAttendee,
    theme, toggleTheme
  } = useAppState();

  const [loader, setLoader] = useState(false);
  const { errorMessage, updateErrorMessage } = useContext(getErrorContext());
  const navigate = useNavigate();
  const logger = useLogger();
  const browserBehavior = new DefaultBrowserBehavior();
  const { classes } = useStyles();
  const dispatch = useNotificationDispatch();

  useEffect(() => {
    if (theme === 'dark') {
      toggleTheme();
    }
  }, []);

  const createGetAttendeeCallback = (meetingId) =>
    async (chimeAttendeeId) => {
      const data = await getAttendee(meetingId, chimeAttendeeId);
      return {
        name: data.AttendeeInfo.Name,
      };
    }

  const handleJoinMeeting = async (data) => {
    // e.preventDefault();
    setLoader(true);
    const id = data?.title.trim().toLocaleLowerCase();
    setMeetingId(id);
    const attendeeName = data?.name.trim();
    setLocalUserName(attendeeName);
    if (!id || !attendeeName) {
      if (!attendeeName) {
        const payload = {
          severity: Severity.ERROR,
          message: "Attendee Name Error",
          autoClose: true,
        };
        dispatch({
          type: ActionType.ADD,
          payload: payload,
        });
        setLoader(false);
      }

      if (!id) {
        const payload = {
          severity: Severity.ERROR,
          message: "Meeting Id Error",
          autoClose: true,
        };
        dispatch({
          type: ActionType.ADD,
          payload: payload,
        });
        setLoader(false);
      }

      return;
    }

    meetingManager.getAttendee = createGetAttendeeCallback(id);

    try {
      const { JoinInfo } = await fetchMeeting(id, attendeeName, region, isEchoReductionEnabled);
      setJoinInfo(JoinInfo);
      setLocalAttendee(JoinInfo?.Attendee);
      const meetingSessionConfiguration = new MeetingSessionConfiguration(JoinInfo?.Meeting, JoinInfo?.Attendee);
      if (
        meetingConfig.postLogger &&
        meetingSessionConfiguration.meetingId &&
        meetingSessionConfiguration.credentials &&
        meetingSessionConfiguration.credentials.attendeeId
      ) {
        const existingMetadata = meetingConfig.postLogger.metadata;
        meetingConfig.postLogger.metadata = {
          ...existingMetadata,
          meetingId: meetingSessionConfiguration.meetingId,
          attendeeId: meetingSessionConfiguration.credentials.attendeeId,
        };
      }

      setRegion(JoinInfo.Meeting.MediaRegion);
      /* please check the prioritybasedpolicy, enablesimulcast, skipDeviceSelection, isWebAudioEnabled and keeplastframepaused in basic git hub */
      //--------------------------------------//
      meetingSessionConfiguration.enableSimulcastForUnifiedPlanChromiumBasedBrowsers = true; // enablesimulcast state
      //Specify the apdative probe downlink policy
      meetingSessionConfiguration.videoDownlinkBandwidthPolicy = new VideoAdaptiveProbePolicy(logger);
      if (browserBehavior.supportDownlinkBandwidthEstimation()) {
        meetingSessionConfiguration.videoDownlinkBandwidthPolicy = new VideoPriorityBasedPolicy(logger); // prioritybasedpolicy state
      }
      meetingSessionConfiguration.keepLastFrameWhenPaused = true; // keepLastFrameWhenPaused state
      const options = {
        deviceLabels: DeviceLabels.AudioAndVideo,
        enableWebAudio: true, // isWebAudioEnabled state
        skipDeviceSelection: false, //skipDeviceSelection state
      };
      //-----------------------------------//

      await meetingManager.join(meetingSessionConfiguration, options);
      // if (meetingMode === MeetingMode.Spectator) {
      //   await meetingManager.start();
      //   navigate(`${routes.MEETING}}/${meetingId}`);
      // } else {
      // setMeetingMode(MeetingMode.Attendee);
      navigate(routes.DEVICE);
      // }
      setLoader(false);
    } catch (error) {
      // updateErrorMessage((error).message);
      // setLoader(false);
      const payload = {
        severity: Severity.ERROR,
        message: (error).message,
        autoClose: true,
      };
      dispatch({
        type: ActionType.ADD,
        payload: payload,
      });
      setLoader(false);
    }
  };

  const closeError = () => {
    updateErrorMessage('');
    setMeetingId('');
    setLocalUserName('');
  };

  return (
    <Grid>
      <Grid>
        <img src={require(`../../images/logo1.png`)} style={{ width: "273px" }} />
        <Typography variant="h6" gutterBottom>
          Join a meeting
        </Typography>
        <EditRecordForm
          form="create_room"
          fields={videoCallForm.filter(_ => _.editRecord)}
          handleSubmit={(e) => handleJoinMeeting(e)}
          btnLabel={"Create Meeting"}
          noNeedCancel={true}
          submitBtn={(props) => {
            // const { submitting, invalid, dirty } = props;
            // console.log("props = ", props);
            return !loader ? <Button
              type="submit"
              variant="contained"
              color="primary"
              // disabled={submitting || !invalid || !dirty}
              className={classes.cancelBtn}>

              Create Meeting
            </Button> :
              <LoadingButton loading variant="outlined" className={classes.loadingBtn} />
          }
          }
        />
        {errorMessage && (
          <Modal size="md" onClose={closeError}>
            <ModalHeader title={`Meeting ID: ${meetingId}`} />
            <ModalBody>
              <Card
                title="Unable to join meeting"
                description="There was an issue finding that meeting. The meeting may have already ended, or your authorization may have expired."
                smallText={errorMessage}
              />
            </ModalBody>
          </Modal>
        )}
        <DevicePermissionPrompt />
      </Grid>
    </Grid>
  );
};


export default MeetingForm;
