
import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    formContainer: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'capitalize',
        fontFamily: 'Avenir-Regular',
        justifyContent: 'center'
    },
    form: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        padding: '18px',
        marginTop: '40px'
    },
    cancelBtn: {
        minWidth: '100px',
        minHeight: '35px',
        marginTop: '20px',
        fontFamily: 'Avenir-Regular',
        fontWeight: 'bold',
        marginRight: '12px',
        paddingLeft: '25px',
        borderRadius: '20px',
        paddingRight: '25px',
        backgroundColor: '#2ca01c'
    },
    loadingBtn: {
        minWidth: '100px',
        minHeight: '35px',
        marginTop: '20px',
        fontFamily: 'Avenir-Regular',
        fontWeight: 'bold',
        marginRight: '12px',
        paddingLeft: '25px',
        borderRadius: '20px',
        paddingRight: '25px',
        backgroundColor: '#2ca01c',
        "& .MuiLoadingButton-loadingIndicator": {
            color: "#FFF"
        },
    },

    meetingProvider: {
        boxShadow: '0px 0px 8px 1px lightgrey',
    }
}));

// TODO jss-to-tss-react codemod: usages of this hook outside of this file will not be converted.
export default useStyles;