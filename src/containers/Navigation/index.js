
import React, { useState } from 'react';
import MeetingRoster from '../MeetingRoster';
import BackgroundChange from '../BackgroundChange';
import Chat from '../Chat';
import { Flex } from 'amazon-chime-sdk-component-library-react';
// import './styles.css';
import { createTheme } from '@mui/material';
import useStyles from './styles';

const NavigationControl = (props) => {
  const { navigationControl } = props;
  const { showBackgroundChange, showRoster, showChat, setImageBlob, imageBlob } = navigationControl;
  const theme = createTheme();
  const { classes } = useStyles();

  const view = () => {
    if (showRoster && showChat) {
      return (
        <Flex>
          <MeetingRoster navigationControl={navigationControl} />
          <Chat navigationControl={navigationControl} />
        </Flex>
      );
    }
    if (showRoster) {
      return <Flex
        // className='rosterGrid'
        className={classes.rightSideGrid}
      // style={{
      //   backgroundColor: theme?.palette?.mode === 'dark' ? "#000" : "#fff",
      //   zIndex: 2000
      // }}
      ><MeetingRoster navigationControl={navigationControl} /></Flex>;
    }
    if (showChat) {
      return <Flex
        // className='chatGrid'
        className={classes.rightSideGrid}
      // style={{
      //   backgroundColor: theme?.palette?.mode === 'dark' ? "#000" : "#fff",
      //   zIndex: 2000
      // }}
      ><Chat navigationControl={navigationControl} /></Flex>;
    }
    if (showBackgroundChange) {
      return (
        <Flex
          // className='rosterGrid'
          className={classes.rightSideGrid}
        >
          <BackgroundChange navigationControl={navigationControl} setImageBlob={setImageBlob} imageBlob={imageBlob} />
        </Flex>);
    }
    return null;
  };

  return (
    <>
      {view()}
    </>
  );
};

export default NavigationControl;
