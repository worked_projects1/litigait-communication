import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme) => ({
    rightSideGrid: {
        boxShadow: "lightgrey 0px 0px 8px 1px",
        marginLeft: "25px",
        borderRadius: "5px",
        backgroundColor: theme?.palette?.mode === 'dark' ? "#000" : "#fff",
        zIndex: 2000,

        '@media(max-width: 35.5rem)': {
            position: "absolute",
            marginLeft: "0px",
            width: "100%",
            height: "100%"
        }
    },
}));

export default useStyles;