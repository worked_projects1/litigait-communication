
import React from 'react';
import { Route, Routes } from 'react-router-dom';
import {
  VoiceFocusTransformDevice,
} from 'amazon-chime-sdk-js';
import {

  MeetingProvider,
  useVoiceFocus,
  VoiceFocusProvider,
  // Severity,
  // ActionType,
  // useNotificationDispatch,
  // useRosterState,
  // useNotificationState
} from 'amazon-chime-sdk-component-library-react';
import NoMeetingRedirect from '../NoMeetingRedirect';
import routes from '../../constants/routes';
import { Meeting, Home, DeviceSetup } from '../../views';
import MeetingEventObserver from '../MeetingEventObserver';
import { useAppState } from '../../providers/AppStateProvider';
// import { VideoFiltersCpuUtilization } from '../../types';
import NavigationProvider from '../../providers/NavigationProvider';
import styled from 'styled-components';
import MeetingStatusNotifier from '../MeetingStatusNotifier';

const MeetingProviderWithDeviceReplacement = ({ children }) => {
  const { addVoiceFocus, isVoiceFocusSupported } = useVoiceFocus();
  const onDeviceReplacement = (
    nextDevice,
    currentDevice
  ) => {
    if (currentDevice instanceof VoiceFocusTransformDevice && isVoiceFocusSupported) {
      return addVoiceFocus(nextDevice);
    }
    return Promise.resolve(nextDevice);
  };

  const meetingConfigValue = {
    onDeviceReplacement,
  };

  return <MeetingProvider {...meetingConfigValue}>{children}</MeetingProvider>;
};

const MeetingProviderWrapper = () => {
  const {
    // isWebAudioEnabled, videoTransformCpuUtilization,
    imageBlob, joinInfo, meetingId } = useAppState();
  // const isFilterEnabled = videoTransformCpuUtilization !== VideoFiltersCpuUtilization.Disabled;
  const getMeetingProviderWrapper = () => {
    return (
      <NavigationProvider>
        <Routes>
          <Route exact path={routes.HOME} element={<Home />} />
          <Route exact path={routes.OTP_FORM} element={<Home />} />
          <Route path={routes.DEVICE} element={
            // <NoMeetingRedirect>
            <DeviceSetup />
            // </NoMeetingRedirect>
          } />
          <Route path={`/meeting/${meetingId}`} element={
            // <NoMeetingRedirect>
            <MeetingModeSelector />
            // </NoMeetingRedirect>
          } />

        </Routes>
        <MeetingEventObserver />
      </NavigationProvider>
    );
  };

  function voiceFocusName(name) {
    if (name && ['default', 'ns_es'].includes(name)) {
      return name;
    }
    return 'default';
  }

  function getVoiceFocusSpecName() {
    if (
      joinInfo &&
      joinInfo.Meeting?.MeetingFeatures?.Audio?.EchoReduction === 'AVAILABLE'
    ) {
      return voiceFocusName('ns_es');
    }
    return voiceFocusName('default');
  }

  const vfConfigValue = {
    spec: { name: getVoiceFocusSpecName() },
    createMeetingResponse: joinInfo,
  };

  const getMeetingProviderWrapperWithVF = (children) => {
    return (
      <VoiceFocusProvider {...vfConfigValue}>
        <MeetingProviderWithDeviceReplacement>
          {children}
        </MeetingProviderWithDeviceReplacement>
      </VoiceFocusProvider>
    );
  };

  // const getWrapperWithVideoFilter = (children) => {
  //   let filterCPUUtilization = parseInt(videoTransformCpuUtilization, 10);
  //   if (!filterCPUUtilization) {
  //     filterCPUUtilization = 40;
  //   }
  //   console.log(`Using ${filterCPUUtilization} background blur and replacement`);
  //   return (
  //     <BackgroundBlurProvider options={{ filterCPUUtilization }} >
  //       <BackgroundReplacementProvider options={{ imageBlob, filterCPUUtilization }} >
  //         {children}
  //       </BackgroundReplacementProvider>
  //     </BackgroundBlurProvider>
  //   );
  // };

  // const getMeetingProvider = (children) => {
  //   return (
  //     <MeetingProvider>
  //       {children}
  //     </MeetingProvider>
  //   );
  // };

  const getMeetingProviderWithFeatures = () => {
    let children = getMeetingProviderWrapper();

    // if (isFilterEnabled) {
    //   children = getWrapperWithVideoFilter(children);
    // }
    // if (isWebAudioEnabled) {
    children = getMeetingProviderWrapperWithVF(children);
    // } else {
    //   children = getMeetingProvider(children);
    // }
    return children;
  };

  return (
    <>
      {imageBlob === undefined ? <div>Loading Assets</div> : getMeetingProviderWithFeatures()}
    </>
  );
};
const StyledLayout = styled.main`
  height: 100%;
  width: 100%;
  display: flex;
`;
const MeetingModeSelector = () => {
  // const { roster } = useRosterState();
  // const dispatch = useNotificationDispatch();

  // useEffect(() => {
  //   let attendees = Object.values(roster);
  //   if (attendees.length == 1) {
  //     // setShowNotofication(true);
  //     const payload = {
  //       severity: Severity.INFO,
  //       message: "Please wait for attorney to join",
  //       autoClose: false,
  //     };
  //     dispatch({
  //       type: ActionType.ADD,
  //       payload: payload,
  //     });
  //   } else {
  //     // const payload = {
  //     //   severity: Severity.INFO,
  //     //   message: "Please wait for attorney to join",
  //     //   autoClose: false,
  //     // };
  //     // dispatch({
  //     //   type: ActionType.REMOVE,
  //     //   payload: payload,
  //     // });
  //     // setShowNotofication(false);
  //   }

  //   return () => {
  //     const payload = {
  //       severity: Severity.INFO,
  //       message: "Please wait for attorney to join",
  //       autoClose: false,
  //     };
  //     dispatch({
  //       type: ActionType.REMOVE_ALL,
  //       payload: payload,
  //     });
  //     // setShowNotofication(false);
  //   }
  // }, [roster]);

  return <StyledLayout>
    <Meeting />
    <MeetingStatusNotifier />
  </StyledLayout>;
};

export default MeetingProviderWrapper;
