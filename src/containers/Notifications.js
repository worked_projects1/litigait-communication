
import React from 'react';
import {
  useNotificationState,
  NotificationGroup,
  useNotificationDispatch,
  ActionType
} from 'amazon-chime-sdk-component-library-react';
import { Slide, Snackbar, Alert } from '@mui/material';

function TransitionLeft(props) {
  return <Slide {...props} direction="down" />;
}

const Notifications = () => {
  const { notifications } = useNotificationState();
  const dispatch = useNotificationDispatch();

  // return notifications.length ? <NotificationGroup /> : null;
  return notifications.length ? notifications.map((e, i) => {
    console.log("id 56 = ", e.id);
    const snackbarProps = {
      open: true,
      ...(e.autoClose && { onClose: () => dispatch({ type: ActionType.REMOVE, payload: e.id }) }),
      TransitionComponent: TransitionLeft,
      ...(e.autoCloseDelay && { autoHideDuration: e.autoCloseDelay }),
      ...(e.autoClose && { autoHideDuration: 4000 })
    };
    return <Snackbar
      key={e.id}
      {...snackbarProps}
      sx={{ height: `${20 * (i + 1)}%` }}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}>
      <Alert
        // onClose={() => dispatch({ type: ActionType.REMOVE, payload: e.id })}
        severity={e.severity}
        sx={{ width: '100%' }}>
        {e.message}
      </Alert>
    </Snackbar>
  }) : null;
};

export default Notifications;

