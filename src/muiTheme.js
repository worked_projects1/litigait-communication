
import React from 'react';
import { ThemeProvider, createTheme, responsiveFontSizes, } from '@mui/material/styles';
import GlobalStyle from './globalStyles';
import { useAppState } from './providers/AppStateProvider';
import CssBaseline from '@mui/material/CssBaseline';

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
    },
    button: {
        textTransform: 'none'
    },
    typography: {
        "fontFamily": "Avenir-Regular",
        "fontSize": 14,
        "fontWeightLight": 300,
        "fontWeightRegular": 400,
        "fontWeightMedium": 500,
        h5: {
            "fontFamily": "Avenir-Bold",
            "textTransform": "Capitalize"
        }
    },
    components: {
        MuiGrid: {
            styleOverrides: {
                root: {
                    backgroundColor: '#000',
                }
            }
        },
        MuiButton: {
            styleOverrides: {
                root: {
                    backgroundColor: '#2ca01c',
                    // borderRadius: '20px',
                    '&:hover': {
                        backgroundColor: '#2ca01c',
                    }
                },
            },
        },
        MuiIconButton: {
            styleOverrides: {
                root: {
                    backgroundColor: '#e95d0c',
                    color: '#fff',
                    border: '1px solid transparent',
                    // borderRadius: '20px',
                    '&:hover': {
                        // backgroundColor: '#e95d0c',
                        // backgroundColor: 'inherit',
                        backgroundColor: "rgb(152 151 151 / 25 %)",
                        // opacity: 0.5,
                        color: '#e95d0c',
                        border: '1px solid #e95d0c'
                    }
                }
            }
        },
        MuiOutlinedInput: {
            styleOverrides: {
                root: {
                    borderColor: "#e95d0c",
                    "&:hover .MuiOutlinedInput-notchedOutline": {
                        borderRadius: 5,
                        borderColor: "#e95d0c",
                        borderWidth: 1
                    },
                    "&:focus .MuiOutlinedInput-notchedOutline": {
                        borderColor: "#e95d0c",
                    },
                    input: {
                        padding: '5px'
                    }
                }
            }
        },
        MuiButtonStartIcon: {
            styleOverrides: {
                root: {
                    backgroundColor: 'transparent',
                }
            }
        },
        MuiAlert: {
            styleOverrides: {
                standardSuccess: {
                    backgroundColor: 'green',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardError: {
                    backgroundColor: 'red',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardWarning: {
                    backgroundColor: 'orange',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardInfo: {
                    backgroundColor: 'grey',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },

            },
        },
    }
});

const lightTheme = createTheme({
    palette: {
        mode: 'light',
    },
    button: {
        textTransform: 'none',
    },
    typography: {
        "fontFamily": "Avenir-Regular",
        "fontSize": 14,
        "fontWeightLight": 300,
        "fontWeightRegular": 400,
        "fontWeightMedium": 500,
        h5: {
            "fontFamily": "Avenir-Bold",
            "textTransform": "Capitalize"
        }
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    backgroundColor: '#2ca01c',
                    // borderRadius: '20px',
                    '&:hover': {
                        backgroundColor: '#2ca01c',
                    }
                },
            },
        },
        MuiIconButton: {
            styleOverrides: {
                root: {
                    backgroundColor: '#e95d0c',
                    color: '#fff',
                    border: '1px solid transparent',
                    // borderRadius: '20px',
                    '&:hover': {
                        // backgroundColor: '#e95d0c',
                        // backgroundColor: 'inherit',
                        backgroundColor: "rgb(152 151 151 / 25 %)",
                        // opacity: 0.5,
                        color: '#e95d0c',
                        border: '1px solid #e95d0c'
                    }
                }
            }
        },
        MuiOutlinedInput: {
            styleOverrides: {
                root: {
                    borderColor: "#e95d0c",
                    "&:hover .MuiOutlinedInput-notchedOutline": {
                        borderRadius: 5,
                        borderColor: "#e95d0c",
                        borderWidth: 1
                    },
                    "&:focus .MuiOutlinedInput-notchedOutline": {
                        borderColor: "#e95d0c",
                    },
                    input: {
                        padding: '5px'
                    }
                }
            }
        },
        MuiAlert: {
            styleOverrides: {
                standardSuccess: {
                    backgroundColor: 'green',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardError: {
                    backgroundColor: 'red',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardWarning: {
                    backgroundColor: 'orange',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },
                standardInfo: {
                    backgroundColor: 'grey',
                    color: 'white',
                    '& .MuiAlert-icon': {
                        color: 'white'
                    }
                },

            },
        },
    }
});


const MUITheme = ({ children }) => {
    const { theme } = useAppState();

    return (
        <ThemeProvider theme={theme === 'light' ? responsiveFontSizes(lightTheme) : responsiveFontSizes(darkTheme)}>
            <GlobalStyle />
            <CssBaseline />
            {children}
        </ThemeProvider>
    );
};

export default MUITheme;