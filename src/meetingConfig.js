

import { ConsoleLogger, MultiLogger, POSTLogger, VideoPriorityBasedPolicy } from 'amazon-chime-sdk-js';
import { SDK_LOG_LEVELS } from './constants';
// import { useLocation } from 'react-router-dom';


const urlParams = new URLSearchParams(window.location.search);
const queryLogLevel = urlParams.get('logLevel') || 'info';
const logLevel = SDK_LOG_LEVELS[queryLogLevel] || SDK_LOG_LEVELS.info;

const meetingConfig = {
  simulcastEnabled: true,
  logger: new ConsoleLogger('ChimeComponentLibraryReactDemo', logLevel),
};

const BASE_URL = [window.location.protocol, '//', window.location.host, window.location.pathname.replace(/\/*$/, '/')].join('');

if (!['0.0.0.0', '127.0.0.1', 'localhost'].includes(window.location.hostname)) {
  const postLogger = new POSTLogger({
    url: `${BASE_URL}logs`,
    logLevel,
    metadata: {
      appName: 'ChimeComponentLibraryReactDemo',
      timestamp: Date.now().toString(), // Add current timestamp for unique AWS CloudWatch log stream generation. This will be unique per POSTLogger creation in time.
    },
  });
  meetingConfig.logger = new MultiLogger(meetingConfig.logger, postLogger);
  meetingConfig.postLogger = postLogger;
  meetingConfig.videoDownlinkBandwidthPolicy = new VideoPriorityBasedPolicy(meetingConfig.logger);

}

export default meetingConfig;
